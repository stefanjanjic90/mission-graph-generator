﻿using System.Collections.Generic;
using MissionGraphGeneratorPlugin.Common;
using MissionGraphGeneratorPlugin.Common.Node;
using MissionGraphGeneratorPlugin.Common.Rule;

namespace MissionGraphGeneratorPlugin.Data
{
    delegate void RuleMapChangeHandler(Rule rule);

    sealed class ApplicationData
    {
        private static readonly ApplicationData instance = new ApplicationData();

        private Dictionary<string, Rule> ruleMap;
        private event RuleMapChangeHandler RuleMapChange;

        private Graph startGraph;
        private int numberOfIterations;
        private bool enableRuleProbability;
        private int isomorphismOption;

        public Graph StartGraph { get { return this.startGraph; } }
        public int NumberOfIterations { get { return this.numberOfIterations; } set { this.numberOfIterations = value; } }
        public bool EnableRuleProbability { get { return this.enableRuleProbability; } set { this.enableRuleProbability = value; } }
        public int IsomorphismOption { get { return this.isomorphismOption; } set { this.isomorphismOption = value; } }

        private ApplicationData()
        {
            this.numberOfIterations = 0;
            this.enableRuleProbability = false;
        }

        public static ApplicationData Instance
        {
            get { return instance; }
        }

        public Dictionary<string, Rule> RuleMap
        {
            get { return this.ruleMap; }
            set { this.ruleMap = value; }
        }

        public void SaveRule(string key, Rule rule)
        {
            this.ruleMap[key] = rule;
            this.RuleMapChange(rule);
        }

        public void RemoveRule(string key)
        {
            Rule removedRule = this.ruleMap[key];
            this.ruleMap.Remove(key);
            this.RuleMapChange(removedRule);
        }

        public void OnRuleMapChange(RuleMapChangeHandler handler)
        {
            this.RuleMapChange += new RuleMapChangeHandler(handler);
        }

        public void SaveStartGraph(Graph startGraph)
        {
            this.startGraph = startGraph;
        }

        public void Load()
        {
            this.ruleMap = new Dictionary<string, Rule>();

            this.startGraph = new Graph();
            this.StartGraph.InsertNode<NonterminalNode>("Start");
        }
    }
}
