﻿using UnityEngine;

namespace MissionGraphGeneratorPlugin.Common.Node
{
    [System.Serializable]
    class TerminalNode : BaseNode
    {
        private NodeColor nodeColor;

        [System.Runtime.Serialization.IgnoreDataMember]
        public override Color Color { get { return new Color(this.nodeColor.Red, this.nodeColor.Green, this.nodeColor.Blue); } }

        protected override string AttributePrefix { get { return "TERMINAL_NODE_"; } }

        public TerminalNode()
        {
            this.nodeColor = new NodeColor(1.0f, 1.0f, 1.0f);
        }

        public override NodeType GetNodeType()
        {
            return NodeType.TerminalNode;
        }
    }
}