﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF;

namespace MissionGraphGeneratorPlugin.Common.Node
{
    [Serializable]
    abstract class BaseNode
    {
        public static readonly int DefaultWidth = 120;
        public static readonly int DefaultHeight = 80;

        public Rectangle Rectangle { get; set; }

        [System.Runtime.Serialization.IgnoreDataMember]
        public Rect WindowRect
        {
            get
            {
                return new Rect(this.Rectangle.X, this.Rectangle.Y, this.Rectangle.Width, this.Rectangle.Height);
            }
            set
            {
                this.Rectangle = new Rectangle(value.x, value.y, value.width, value.height);
            }
        }

        [System.Runtime.Serialization.IgnoreDataMember]
        public abstract Color Color { get; }

        private int id = Graph.IllegalNodeId;
        private string name = null;
        private SortedList<int, Edge> outboundEdges = new SortedList<int, Edge>();
        private List<Edge> inboundEdges = new List<Edge>();

        protected abstract string AttributePrefix { get; }

        public int Id { get { return this.id; } set { this.id = value; } }
        public string Attribute { get { return this.AttributePrefix + this.name; } }
        public string Name { get { return this.name; } set { this.name = value; } }
        public SortedList<int, Edge> OutboundEdges { get { return this.outboundEdges; } set { this.outboundEdges = value; } }
        public List<Edge> InboundEdges { get { return this.inboundEdges; } set { this.inboundEdges = value; } }

        public Edge FindOutEdge(int nodeIdTo)
        {
            try
            {
                return outboundEdges[nodeIdTo];
            }
            catch (Exception)
            {
                VFException.Error("Inconsistent Data");
            }
            return null;
        }

        public virtual void DrawWindowContent()
        {
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Name");
            this.name = EditorGUILayout.TextField(this.name);

            EditorGUILayout.EndVertical();
        }

        public abstract NodeType GetNodeType();
    }

    enum NodeType
    {
        NonterminalNode,
        TerminalNode,
        JokerNode
    }
}
