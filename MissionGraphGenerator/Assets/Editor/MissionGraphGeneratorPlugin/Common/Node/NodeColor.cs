﻿namespace MissionGraphGeneratorPlugin.Common.Node
{
    [System.Serializable]
    class NodeColor
    {
        private float red;
        private float green;
        private float blue;

        public float Red { get { return this.red; } set { this.red = value; } }
        public float Green { get { return this.green; } set { this.green = value; } }
        public float Blue { get { return this.blue; } set { this.blue = value; } }

        public NodeColor() { this.red = 0.0f; this.green = 0.0f; this.blue = 0.0f; }
        public NodeColor(float red, float green, float blue) { this.red = red; this.green = green; this.blue = blue; }
    }
}
