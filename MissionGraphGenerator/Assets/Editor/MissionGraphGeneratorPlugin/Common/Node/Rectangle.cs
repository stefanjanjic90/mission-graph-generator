﻿
namespace MissionGraphGeneratorPlugin.Common.Node
{
    [System.Serializable]
    public class Rectangle
    {
        private float x;
        private float y;
        private float width;
        private float height;

        public float X
        {
            get { return this.x; }
            set { this.x = value; }
        }
        public float Y
        {
            get { return this.y; }
            set { this.y = value; }
        }
        public float Width
        {
            get { return this.width; }
            set { this.width = value; }
        }
        public float Height
        {
            get { return this.height; }
            set { this.height = value; }
        }

        public Rectangle(float x, float y, float width, float height)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }
}
