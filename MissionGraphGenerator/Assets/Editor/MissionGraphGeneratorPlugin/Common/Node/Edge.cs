﻿namespace MissionGraphGeneratorPlugin.Common.Node
{
    [System.Serializable]
    class Edge
    {
        private int nodeIdFrom = Graph.IllegalNodeId;
        private int nodeIdTo = Graph.IllegalNodeId;
        private string attribute = null;

        public int NodeIdFrom { get { return this.nodeIdFrom; } set { this.nodeIdFrom = value; } }
        public int NodeIdTo { get { return this.nodeIdTo; } set { this.nodeIdTo = value; } }
        public string Attribute { get { return this.attribute; } set { this.attribute = value; } }

        public bool Equal(Edge edge)
        {
            return this.nodeIdFrom == edge.nodeIdFrom && this.nodeIdTo == edge.nodeIdTo;
        }
    }
}
