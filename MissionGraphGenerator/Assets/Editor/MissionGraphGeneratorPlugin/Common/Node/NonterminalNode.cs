﻿using UnityEngine;

namespace MissionGraphGeneratorPlugin.Common.Node
{
    [System.Serializable]
    class NonterminalNode : BaseNode
    {
        private NodeColor nodeColor;

        [System.Runtime.Serialization.IgnoreDataMember]
        public override Color Color { get { return new Color(this.nodeColor.Red, this.nodeColor.Green, this.nodeColor.Blue); } }

        protected override string AttributePrefix { get { return "NON_TERMINAL_NODE_"; } }

        public NonterminalNode()
        {
            this.nodeColor = new NodeColor(0.0f, 1.0f, 0.0f);
        }

        public override NodeType GetNodeType()
        {
            return NodeType.NonterminalNode;
        }
    }
}