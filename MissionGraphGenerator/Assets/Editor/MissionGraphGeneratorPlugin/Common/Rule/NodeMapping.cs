﻿using MissionGraphGeneratorPlugin.Common.Node;

namespace MissionGraphGeneratorPlugin.Common.Rule
{
    [System.Serializable]
    class NodeMapping
    {
        private BaseNode lhsNode;
        private BaseNode rhsNode;

        public BaseNode LhsNode { get { return this.lhsNode; } set { this.lhsNode = value; } }
        public BaseNode RhsNode { get { return this.rhsNode; } set { this.rhsNode = value; } }

        public NodeMapping() { }

        public NodeMapping(BaseNode lhsNode, BaseNode rhsNode)
        {
            this.lhsNode = lhsNode;
            this.rhsNode = rhsNode;
        }

    }
}
