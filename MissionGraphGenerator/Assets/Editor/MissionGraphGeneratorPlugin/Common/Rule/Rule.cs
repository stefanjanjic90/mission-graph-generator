﻿using MissionGraphGeneratorPlugin.Common.Node;
using System.Collections.Generic;

namespace MissionGraphGeneratorPlugin.Common.Rule
{
    [System.Serializable]
    class Rule
    {
        private string _name;
        private Graph leftHandSide;
        private Graph rightHandSide;
        private float probability;
        private List<NodeMapping> nodeMappings;

        public string Name { get { return this._name; } set { this._name = value; } }
        public Graph LeftHandSide { get { return this.leftHandSide; } set { this.leftHandSide = value; } }
        public Graph RightHandSide { get { return this.rightHandSide; } set { this.rightHandSide = value; } }
        public float Probability { get { return this.probability; } set { this.probability = value; } }
        public List<NodeMapping> NodeMappings { get { return this.nodeMappings; } }

        public Rule()
        {
            this._name = "";
            this.leftHandSide = new Graph();
            this.rightHandSide = new Graph();
            this.nodeMappings = new List<NodeMapping>();
        }

        public void AddNodeMapping(BaseNode lhsNode, BaseNode rhsNode)
        {
            NodeMapping nodeMapping = new NodeMapping(lhsNode, rhsNode);
            this.nodeMappings.Add(nodeMapping);
        }

        public void RemoveNodeMapping(NodeMapping nodeMapping)
        {
            this.nodeMappings.Remove(nodeMapping);
        }

        public bool IsNodeMapped(BaseNode baseNode, RuleSide ruleSide)
        {
            bool isNodeMapped = false;
            foreach (NodeMapping nodeMapping in this.nodeMappings)
            {
                if (ruleSide == RuleSide.Left)
                {
                    isNodeMapped = isNodeMapped || nodeMapping.LhsNode.Id == baseNode.Id;
                }
                else if (ruleSide == RuleSide.Right)
                {
                    isNodeMapped = isNodeMapped || nodeMapping.RhsNode.Id == baseNode.Id;
                }
                else
                {
                    throw new System.Exception("Unknown Node Side.");
                }
            }

            return isNodeMapped;
        }

        public BaseNode FindRhsNode(BaseNode lhsNode)
        {
            BaseNode rhsNode = null;
            foreach (NodeMapping nodeMapping in this.nodeMappings)
            {
                if (nodeMapping.LhsNode.Id == lhsNode.Id)
                {
                    rhsNode = nodeMapping.RhsNode;
                    break;
                }

            }

            return rhsNode;
        }

        public BaseNode FindLhsNode(BaseNode rhsNode)
        {
            BaseNode lhsNode = null;
            foreach (NodeMapping nodeMapping in this.nodeMappings)
            {
                if (nodeMapping.RhsNode.Id == rhsNode.Id)
                {
                    lhsNode = nodeMapping.LhsNode;
                    break;
                }
            }
            return lhsNode;
        }

    }

    public enum RuleSide
    {
        Left = 0,
        Right = 1
    }
}
