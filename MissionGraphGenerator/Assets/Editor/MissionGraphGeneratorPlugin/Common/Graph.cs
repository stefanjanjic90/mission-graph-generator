﻿using System;
using System.Collections.Generic;
using MissionGraphGeneratorPlugin.Common.Node;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF;
using UnityEngine;
using System.Linq;

namespace MissionGraphGeneratorPlugin.Common
{
    [Serializable]
    class Graph : IGraph
    {
        public static readonly int IllegalNodeId = -1;

        private SortedList<int, BaseNode> nodes;

        public SortedList<int, BaseNode> Nodes
        {
            get { return this.nodes; }
            set { this.nodes = value; }
        }

        public Graph()
        {
            this.nodes = new SortedList<int, BaseNode>();
        }

        public int NodeCount { get { return this.nodes.Count; } }

        public BaseNode FindNode(int id)
        {
            int i = this.nodes.IndexOfKey(id);
            if (i >= 0)
            {
                return this.nodes.Values[i];
            }
            VFException.Error("Finde node - Inconsistent data");
            return null;
        }

        public int NodeIdFromNodeIndex(int nodeIndex) { return this.nodes.Values[nodeIndex].Id; }
        public int NodeIndexFromNodeId(int nodeId) { return this.nodes.IndexOfKey(nodeId); }
        public string GetNodeAttribute(int nodeId) { return this.FindNode(nodeId).Attribute; }
        public bool IsJoker(int nodeId) { return this.FindNode(nodeId).GetNodeType() == NodeType.JokerNode; }
        public int InEdgeCount(int nodeId) { return this.FindNode(nodeId).InboundEdges.Count; }
        public int OutEdgeCount(int nodeId) { return this.FindNode(nodeId).OutboundEdges.Count; }

        public int GetInEdge(int nodeIdTo, int nodeIndexEdge, out string attribute)
        {
            Edge edge = null;
            try
            {
                edge = this.FindNode(nodeIdTo).InboundEdges[nodeIndexEdge];
            }
            catch (Exception)
            {
                VFException.Error("GetInEdge - Inconsistent data");
            }

            attribute = edge.Attribute;
            return edge.NodeIdFrom;
        }

        public int GetOutEdge(int nodeIdFrom, int nodeIndexEdge, out string attribute)
        {
            Edge edge = null;
            try
            {
                edge = FindNode(nodeIdFrom).OutboundEdges.Values[nodeIndexEdge];
            }
            catch (Exception)
            {
                VFException.Error("GetOutEdge - Inconsistent data");
            }

            attribute = edge.Attribute;
            return edge.NodeIdTo;
        }

        public int InsertNode<T>() where T : BaseNode, new() { return InsertNode<T>(null); }
        public int InsertNode<T>(string name) where T : BaseNode, new() { return InsertNode<T>(name, new Rectangle(0, 0, BaseNode.DefaultWidth, BaseNode.DefaultHeight)); }
        public int InsertNode<T>(string name, Rectangle rectangle) where T : BaseNode, new()
        {
            int id = this.nodes.Count > 0 ? this.nodes.Last().Key + 1 : 0;
            T node = new T
            {
                Id = id,
                Name = name,
                Rectangle = rectangle
            };
            this.nodes.Add(node.Id, node);
            return node.Id;
        }

        public int InsertNode(BaseNode baseNode)
        {
            int nodeId;
            switch (baseNode.GetNodeType())
            {
                case NodeType.TerminalNode:
                    nodeId = this.InsertNode<TerminalNode>(baseNode.Name, baseNode.Rectangle);
                    break;
                case NodeType.NonterminalNode:
                    nodeId = this.InsertNode<NonterminalNode>(baseNode.Name, baseNode.Rectangle);
                    break;
                case NodeType.JokerNode:
                    nodeId = this.InsertNode<JokerNode>(baseNode.Name, baseNode.Rectangle);
                    break;
                default:
                    throw new SystemException("Unknown Node Type.");

            }

            return nodeId;
        }



        public bool EdgeExists(int nodeIdFrom, int nodeIdTo)
        {
            BaseNode nodeFrom = this.FindNode(nodeIdFrom);
            return nodeFrom.OutboundEdges.ContainsKey(nodeIdTo);
        }

        public bool EdgeExists(Edge edge)
        {
            BaseNode nodeFrom = this.FindNode(edge.NodeIdFrom);
            return nodeFrom.OutboundEdges.ContainsKey(edge.NodeIdTo);
        }

        public void InsertEdge(int nodeIdFrom, int nodeIdTo) { InsertEdge(nodeIdFrom, nodeIdTo, null); }
        public void InsertEdge(int nodeIdFrom, int nodeIdTo, string attribute)
        {
            if (this.EdgeExists(nodeIdFrom, nodeIdTo))
            {
                Debug.LogWarning("Tried to add already existing edge. From:" + nodeIdFrom + " - To:" + nodeIdTo);
                return;
            }

            Edge edge = new Edge();
            BaseNode nodeFrom = this.FindNode(nodeIdFrom);
            BaseNode nodeTo = this.FindNode(nodeIdTo);

            edge.NodeIdFrom = nodeIdFrom;
            edge.NodeIdTo = nodeIdTo;
            edge.Attribute = attribute;
            try
            {
                nodeFrom.OutboundEdges.Add(nodeIdTo, edge);
                nodeTo.InboundEdges.Add(edge);
            }
            catch (Exception)
            {
                VFException.Error("Inconsistent Data");
            }
        }

        public void DeleteNode(int nodeId)
        {
            BaseNode node = this.FindNode(nodeId);
            Edge[] arend = new Edge[node.OutboundEdges.Count + node.InboundEdges.Count];
            node.OutboundEdges.Values.CopyTo(arend, 0);
            node.InboundEdges.CopyTo(arend, node.OutboundEdges.Count);

            foreach (Edge end in arend)
            {
                DeleteEdge(end.NodeIdFrom, end.NodeIdTo);
            }
            this.nodes.Remove(node.Id);
        }

        public void DeleteEdge(int nodeIdFrom, int nodeIdTo)
        {
            BaseNode nodeFrom = FindNode(nodeIdFrom);
            BaseNode nodeTo = FindNode(nodeIdTo);
            Edge edge = nodeFrom.FindOutEdge(nodeIdTo);

            nodeFrom.OutboundEdges.Remove(nodeIdTo);
            nodeTo.InboundEdges.Remove(edge);
        }

    }
}
