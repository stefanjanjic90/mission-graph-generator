﻿using System;
using System.Collections.Generic;
using MissionGraphGeneratorPlugin.Common;
using MissionGraphGeneratorPlugin.Common.Rule;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF;
using UnityEngine;
using MissionGraphGeneratorPlugin.Utility;
using MissionGraphGeneratorPlugin.Common.Node;

namespace MissionGraphGeneratorPlugin.DPO
{
    class DPOController
    {
        private Graph resultGraph;
        private Dictionary<string, Rule> ruleMap;
        private int rewritingIterations;
        private bool enableRuleProbability;
        private int isomorphismOption;
        private int executedRewritingIterations;
        private Rule lastAppliedRule;

        private Queue<Rule> ruleQueue;

        public Graph ResultGraph { get { return this.resultGraph; } }

        public DPOController(Graph startGraph, Dictionary<string, Rule> ruleMap, int rewritingIterations = 0, bool enableRuleProbability = false, int isomorphismOption = 0)
        {
            this.resultGraph = DeepCloner.Clone<Graph>(startGraph);
            this.ruleMap = ruleMap;
            this.rewritingIterations = rewritingIterations;
            this.enableRuleProbability = enableRuleProbability;
            this.isomorphismOption = isomorphismOption;
            this.executedRewritingIterations = 0;

            if (!enableRuleProbability)
            {
                this.ruleQueue = new Queue<Rule>();
                foreach (KeyValuePair<string, Rule> keyRule in this.ruleMap)
                {
                    this.ruleQueue.Enqueue(keyRule.Value);
                }
            }
        }

        public void Execute()
        {
            while (true)
            {
                bool iterationsExausted;
                this.ExecuteIteration(out iterationsExausted);
                if (iterationsExausted)
                {
                    break;
                }
            }
        }

        private void ExecuteIteration(out bool iterationsExausted)
        {
            iterationsExausted = false;

            if (this.rewritingIterations != 0 && this.rewritingIterations == this.executedRewritingIterations)
            {
                iterationsExausted = true;
                Debug.Log("After executing " + executedRewritingIterations + "iteratons. Algorithm found no valid isomporphism.");
                return;
            }

            int[] mapping;
            Rule rule = this.GetRewritingRule(out mapping);
            if (rule != null)
            {
                this.Substitute(rule, mapping);
                this.executedRewritingIterations++;
            }
            else
            {
                Debug.Log("After executing " + executedRewritingIterations + "iteratons. Algorithm found no valid isomporphism.");
                iterationsExausted = true;
            }
        }

        private Rule GetRewritingRule(out int[] mapping)
        {
            Rule rule = null;
            bool lastAppliedRuleIsomorphismFound = false;

            // find isomorphisam for a rule that was applied in previous iteration -- case when finding all available isomorphisms in graph for specific rule
            if (isomorphismOption == 1 && this.lastAppliedRule != null)
            {
                lastAppliedRuleIsomorphismFound = this.FindSubgraph(this.resultGraph, lastAppliedRule.LeftHandSide, out mapping);

                if (lastAppliedRuleIsomorphismFound)
                {
                    rule = this.lastAppliedRule;
                }
            }

            if ((!lastAppliedRuleIsomorphismFound && isomorphismOption == 1) || isomorphismOption == 0)
            {
                if (enableRuleProbability)
                {
                    rule = this.GetProbabilityRule(out mapping);
                }
                else
                {
                    rule = this.GetInOrderRule(out mapping);
                }
                if (isomorphismOption == 1)
                {
                    lastAppliedRule = rule;
                }
            }
            else
            {
                throw new Exception("Unknown isomorphism option");
            }
            return rule;
        }

        private Rule GetProbabilityRule(out int[] mapping)
        {
            Dictionary<string, float> probabilityDictionary = new Dictionary<string, float>();
            // polpulate probability dictionary and probability sum
            foreach (KeyValuePair<string, Rule> keyRule in this.ruleMap)
            {
                probabilityDictionary.Add(keyRule.Key, keyRule.Value.Probability);
            }

            bool isomorphismFound = false;
            Rule isomorphicRule = null;
            mapping = null;
            while (!isomorphismFound && probabilityDictionary.Count > 0)
            {
                // calculate probablity sum
                float totalProbabilitySum = 0;
                foreach (KeyValuePair<string, float> keyProbability in probabilityDictionary)
                {
                    totalProbabilitySum += keyProbability.Value;
                }

                string ruleKey = "";
                Rule rule = null;
                //get a random value
                float randomValue = UnityEngine.Random.value * totalProbabilitySum;

                int ruleCount = probabilityDictionary.Count;
                int currentRule = 0;
                foreach (KeyValuePair<string, float> keyProbability in probabilityDictionary)
                {
                    currentRule++;

                    randomValue -= keyProbability.Value;
                    if (randomValue < 0 || (currentRule == ruleCount))
                    {
                        ruleKey = keyProbability.Key;
                        rule = this.ruleMap[keyProbability.Key];
                        break;
                    }
                }

                isomorphismFound = this.FindSubgraph(this.resultGraph, rule.LeftHandSide, out mapping);
                if (isomorphismFound)
                {
                    isomorphicRule = rule;
                }
                else
                {
                    probabilityDictionary.Remove(ruleKey);
                }
            }

            return isomorphicRule;
        }

        private Rule GetInOrderRule(out int[] mapping)
        {
            mapping = null;
            Rule isomorphicRule = null;
            Rule rule = null;
            bool isomorphismFound = false;
            Queue<Rule> processedRulesQueue = new Queue<Rule>();

            while (!isomorphismFound)
            {
                rule = this.ruleQueue.Dequeue();
                isomorphismFound = this.FindSubgraph(this.resultGraph, rule.LeftHandSide, out mapping);
                processedRulesQueue.Enqueue(rule);

                if (isomorphismFound)
                {
                    isomorphicRule = rule;
                    while (processedRulesQueue.Count > 0)
                    {
                        this.ruleQueue.Enqueue(processedRulesQueue.Dequeue());
                    }
                    break;
                }
                else if (this.ruleQueue.Count == 0)
                {
                    break;
                }
            }

            return isomorphicRule;
        }

        private void Substitute(Rule rule, int[] mapping)
        {
            // iterate through mapping of rule LHS to subject graph
            // find nodes that need to be removed (exist on the LHS but not on RHS)
            // or find edges that need to be removed or added for existing nodes (nodes that are on both sides of the rule)
            for (int i = 0; i < mapping.Length; i++)
            {
                int lhsNodeId = rule.LeftHandSide.NodeIdFromNodeIndex(i);
                Debug.LogWarning("Node Graph1: " + this.resultGraph.FindNode(mapping[i]).Attribute);
                Debug.LogWarning("Node Graph2: " + rule.LeftHandSide.FindNode(lhsNodeId).Attribute);

                BaseNode graphNode = this.resultGraph.FindNode(mapping[i]);
                BaseNode lhsNode = rule.LeftHandSide.FindNode(lhsNodeId);
                BaseNode rhsNode = rule.FindRhsNode(lhsNode);

                // remove node that do not exist on right side
                if (rhsNode == null)
                {
                    this.resultGraph.DeleteNode(graphNode.Id);
                }
                else
                {
                    DetermineNodeEdges(rule, mapping, graphNode, lhsNode, rhsNode);
                }
            }

            // iterate through rhs rule nodes
            // insert into graph nodes that need to be added (nodes that exist on the right side of the rule and not on the left)
            Dictionary<int, Tuple<BaseNode, BaseNode>> rhsNodeGraphNodeDictionary = new Dictionary<int, Tuple<BaseNode, BaseNode>>();
            foreach (KeyValuePair<int, BaseNode> keyRhsNode in rule.RightHandSide.Nodes)
            {
                BaseNode rhsNode = keyRhsNode.Value;
                // if it is a new node add it to subject graph (node is not mapped from right side)
                if (!rule.IsNodeMapped(rhsNode, RuleSide.Right))
                {
                    int addedNodeId = this.resultGraph.InsertNode(rhsNode);
                    Tuple<BaseNode, BaseNode> tuple = Tuple.Create(rhsNode, this.resultGraph.FindNode(addedNodeId));
                    rhsNodeGraphNodeDictionary.Add(rhsNode.Id, tuple);
                }
            }

            // insert edges for newly added nodes
            foreach (KeyValuePair<int, Tuple<BaseNode, BaseNode>> keyTuple in rhsNodeGraphNodeDictionary)
            {
                BaseNode rhsNode = keyTuple.Value.Item1;
                BaseNode addedGraphNode = keyTuple.Value.Item2;

                foreach (KeyValuePair<int, Edge> keyEdge in rhsNode.OutboundEdges)
                {
                    Edge edge = keyEdge.Value;
                    BaseNode lhsNode = rule.FindLhsNode(rule.RightHandSide.FindNode(edge.NodeIdTo));
                    if (lhsNode != null)
                    {
                        int lhsNodeIndex = rule.LeftHandSide.NodeIndexFromNodeId(lhsNode.Id);
                        BaseNode graphNode = this.resultGraph.FindNode(mapping[lhsNodeIndex]);
                        this.resultGraph.InsertEdge(addedGraphNode.Id, graphNode.Id);
                        this.resultGraph.InsertEdge(graphNode.Id, addedGraphNode.Id);
                    }
                    else
                    {
                        Tuple<BaseNode, BaseNode> tuple = rhsNodeGraphNodeDictionary[rule.RightHandSide.FindNode(edge.NodeIdTo).Id];
                        BaseNode graphNode = tuple.Item2;
                        this.resultGraph.InsertEdge(addedGraphNode.Id, graphNode.Id);
                        this.resultGraph.InsertEdge(graphNode.Id, addedGraphNode.Id);
                    }
                }
            }
        }

        private void DetermineNodeEdges(Rule rule, int[] mapping, BaseNode graphNode, BaseNode lhsNode, BaseNode rhsNode)
        {
            // find edges that do not exist on right side and remove them from subject graph
            foreach (KeyValuePair<int, Edge> keyEdge in lhsNode.OutboundEdges)
            {
                Edge edge = keyEdge.Value;
                BaseNode rhsToNode = rule.FindRhsNode(rule.LeftHandSide.FindNode(edge.NodeIdTo));
                int lhsNodeIndexTo = rule.LeftHandSide.NodeIndexFromNodeId(edge.NodeIdTo);
                if (rhsToNode != null && !rule.RightHandSide.EdgeExists(rhsNode.Id, rhsToNode.Id))
                {
                    // remove edge from subject graph 
                    // outbound edges will be deleted from both sides, since we are iterating through each node in mapping
                    this.resultGraph.DeleteEdge(graphNode.Id, this.resultGraph.FindNode(mapping[lhsNodeIndexTo]).Id);
                }
            }

            // find edges that exist on RHS but not on LHS and add them to subject graph
            foreach (KeyValuePair<int, Edge> keyEdge in rhsNode.OutboundEdges)
            {
                Edge edge = keyEdge.Value;
                BaseNode lhsToNode = rule.FindLhsNode(rule.RightHandSide.FindNode(edge.NodeIdTo));
                if (lhsToNode != null && !rule.LeftHandSide.EdgeExists(lhsNode.Id, lhsToNode.Id))
                {
                    // add edge to subject graph 
                    // from boths sides since it is undirected graph
                    int lhsNodeIndexTo = rule.LeftHandSide.NodeIndexFromNodeId(lhsToNode.Id);
                    this.resultGraph.InsertEdge(graphNode.Id, this.resultGraph.FindNode(mapping[lhsNodeIndexTo]).Id);
                    this.resultGraph.InsertEdge(this.resultGraph.FindNode(mapping[lhsNodeIndexTo]).Id, graphNode.Id);
                }
            }
        }

        private bool FindSubgraph(Graph graph, Graph ruleGraph, out int[] mapping)
        {
            VFState vfState = new VFState(graph, ruleGraph);

            bool isomorphismFound = vfState.Match();
            mapping = vfState.Mapping2To1;

            return isomorphismFound;
        }
    }
}
