﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;
using MissionGraphGeneratorPlugin.Common;
using MissionGraphGeneratorPlugin.Data;
using MissionGraphGeneratorPlugin.Utility;

namespace MissionGraphGeneratorPlugin.GraphEditor
{
    class StartGraphEditorWindow : EditorWindow
    {
        private GraphEditorWindow graphEditorWindow;
        private Graph startGraph;
        private bool isUILoaded = false;
        public static void ShowWindow()
        {
            StartGraphEditorWindow startGraphEditorWindow = EditorWindow.GetWindow<StartGraphEditorWindow>("Start Graph Editor");

            Rect startGraphEditorWindowPosition = new Rect()
            {
                x = 0,
                y = 0
            };
            startGraphEditorWindow.position = startGraphEditorWindowPosition;

            if (ApplicationData.Instance.StartGraph != null)
            {
                startGraphEditorWindow.startGraph = DeepCloner.Clone<Graph>(ApplicationData.Instance.StartGraph);
            }
            else
            {
                startGraphEditorWindow.startGraph = new Graph();
            }
        }

        public virtual void OnDestroy()
        {
            this.CloseSubWinows();
        }

        protected virtual void OnGUI()
        {
            this.UILoad();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Actions:");

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Save"))
            {
                this.SaveStartGraph();
            }
            if (GUILayout.Button("Cancel"))
            {
                this.Close();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        private void UILoad()
        {
            if (!this.isUILoaded)
            {
                this.isUILoaded = true;
                this.CreateSubWindows();
                this.SetWindowSizes();
                this.DockSubWindows();
            }
        }

        private void SaveStartGraph()
        {
            StartGraphValidator startGraphValidator = StartGraphValidator.GetValidator();

            StartGraphErrors startGraphErrors = startGraphValidator.Validate(this.startGraph);

            if (startGraphErrors.HasErrors())
            {
                this.DisplayErrorDialog(startGraphErrors);
                return;
            }

            ApplicationData.Instance.SaveStartGraph(this.startGraph);
            this.Close();
        }

        private void DisplayErrorDialog(StartGraphErrors startGraphErrors)
        {
            StringBuilder errorString = new StringBuilder("Rule is invalid due to following problems:\n");

            int errorNumber = 1;
            foreach (KeyValuePair<StartGraphErrorCode, string> entry in startGraphErrors.Errors)
            {
                errorString.Append(errorNumber + ". " + entry.Value);
                errorString.Append("\n");
                errorNumber++;
            }

            EditorUtility.DisplayDialog("Rule Invalid", errorString.ToString(), "OK");

        }

        private void CreateSubWindows()
        {
            // result window
            this.graphEditorWindow = ScriptableObject.CreateInstance<GraphEditorWindow>();
            this.graphEditorWindow.titleContent = new GUIContent("Start Graph");
            this.graphEditorWindow.Init(this.startGraph);
            this.graphEditorWindow.IsJokerNodeOptionEnabled = false;
            this.graphEditorWindow.Show();
        }

        private void DockSubWindows()
        {
            Docker.Dock(this, this.graphEditorWindow, Docker.DockPosition.Right);
        }

        private void SetWindowSizes()
        {
            int widthMarginPx = 10;
            int heightMarginPx = 100;

            this.minSize = new Vector2(Screen.currentResolution.width / 5 - widthMarginPx, Screen.currentResolution.height * 4 / 5 - heightMarginPx);
            this.graphEditorWindow.minSize = new Vector2(Screen.currentResolution.width * 3 / 5 - widthMarginPx, Screen.currentResolution.height * 4 / 5 - heightMarginPx);
        }

        private void CloseSubWinows()
        {
            this.graphEditorWindow.Close();
        }
    }
}
