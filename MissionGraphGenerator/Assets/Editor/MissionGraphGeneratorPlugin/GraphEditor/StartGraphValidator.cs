﻿using System.Collections.Generic;
using MissionGraphGeneratorPlugin.Common;
using MissionGraphGeneratorPlugin.Common.Node;

namespace MissionGraphGeneratorPlugin.GraphEditor
{
    class StartGraphValidator
    {
        private StartGraphValidator()
        {
        }

        public static StartGraphValidator GetValidator()
        {
            return new StartGraphValidator();
        }

        public StartGraphErrors Validate(Graph graph)
        {
            StartGraphErrors startGraphErrors = new StartGraphErrors();

            this.ValidateEmptyStartGraph(graph, startGraphErrors);
            this.ValidateNonterminalNodes(graph, startGraphErrors);

            return startGraphErrors;
        }

        private void ValidateEmptyStartGraph(Graph graph, StartGraphErrors startGraphErrors)
        {
            if (graph.Nodes == null || graph.Nodes.Count == 0)
            {
                startGraphErrors.AddError(StartGraphErrorCode.StartGraphEmpty);
            }
        }

        private void ValidateNonterminalNodes(Graph graph, StartGraphErrors startGraphErrors)
        {
            bool hasNonterminalNode = false;
            foreach (KeyValuePair<int, BaseNode> keyValuePair in graph.Nodes)
            {
                if (keyValuePair.Value.GetNodeType() == NodeType.NonterminalNode)
                {
                    hasNonterminalNode = true;
                    break;
                }
            }

            if (!hasNonterminalNode)
            {
                startGraphErrors.AddError(StartGraphErrorCode.NonterminalNodeNotFound);
            }
        }
    }

    class StartGraphErrors
    {
        private Dictionary<StartGraphErrorCode, string> errors;

        public Dictionary<StartGraphErrorCode, string> Errors { get { return this.errors; } }

        public StartGraphErrors()
        {
            this.errors = new Dictionary<StartGraphErrorCode, string>();
        }

        public void AddError(StartGraphErrorCode startGraphErrorCode)
        {
            this.errors.Add(startGraphErrorCode, startGraphErrorCode.Message());
        }

        public bool HasErrors()
        {
            return this.errors.Count > 0;
        }
    }

    public enum StartGraphErrorCode
    {
        StartGraphEmpty,
        NonterminalNodeNotFound
    }

    public static class StartGraphErrorCodeExtension
    {
        public static string Message(this StartGraphErrorCode startGraphErrorCode)
        {
            switch (startGraphErrorCode)
            {
                case StartGraphErrorCode.StartGraphEmpty:
                    return "Start graph is empty.";
                case StartGraphErrorCode.NonterminalNodeNotFound:
                    return "Start graph must habe at least one nonterminal node.";
                default:
                    return "Unknow error.";
            }
        }
    }
}
