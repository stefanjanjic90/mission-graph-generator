﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using MissionGraphGeneratorPlugin.InputEvent;
using MissionGraphGeneratorPlugin.Common;
using MissionGraphGeneratorPlugin.Common.Node;

namespace MissionGraphGeneratorPlugin.GraphEditor
{
    class GraphEditorWindow : EditorWindow
    {
        private Graph graph = new Graph();
        public Graph Graph { get { return this.graph; } set { this.graph = value; Repaint(); } }

        private BaseNode selectedNode;
        public BaseNode SelectedNode { get { return this.selectedNode; } }

        private bool transitionMode = false;

        private bool isEditModeEnabled;
        private bool isNonTerminalNodeOptionEnabled;
        private bool isTerminalNodeOptionEnabled;
        private bool isJokerNodeOptionEnabled;

        public bool IsEditModeEnabled { get { return this.isEditModeEnabled; } set { this.isEditModeEnabled = value; } }
        public bool IsNonTerminalNodeOptionEnabled { get { return this.isNonTerminalNodeOptionEnabled; } set { this.isNonTerminalNodeOptionEnabled = value; } }
        public bool IsTerminalNodeOptionEnabled { get { return this.isTerminalNodeOptionEnabled; } set { this.isTerminalNodeOptionEnabled = value; } }
        public bool IsJokerNodeOptionEnabled { get { return this.isJokerNodeOptionEnabled; } set { this.isJokerNodeOptionEnabled = value; } }

        public Dictionary<EventType, Action> EventMap { get; set; }

        protected GraphEditorWindow()
        {
            this.isEditModeEnabled = true;
            this.isNonTerminalNodeOptionEnabled = true;
            this.isTerminalNodeOptionEnabled = true;
            this.isJokerNodeOptionEnabled = true;

            this.EventMap = new Dictionary<EventType, Action>
            {
                {EventType.ContextClick, this.OnContext },
                {EventType.Layout, this.OnLayout },
                {EventType.Repaint, this.OnRepaint },
                {EventType.KeyDown, () => {
                    this.OnKeyDown(new Keyboard(Event.current));
                }},
                { EventType.KeyUp, () => {
                    this.OnKeyUp(new Keyboard(Event.current));
                }},
                { EventType.MouseDown, () => {
                    this.OnMouseDown((MouseButton)Event.current.button, Event.current.mousePosition, Event.current);
                }},
                { EventType.MouseDrag, () => {
                    this.OnMouseDrag((MouseButton)Event.current.button, Event.current.mousePosition, Event.current.delta,  Event.current);
                }},

                { EventType.MouseMove, () => {
                    this.OnMouseMove(Event.current.mousePosition, Event.current.delta,  Event.current);
                }},

                { EventType.ScrollWheel, () => {
                    this.OnScrollWheel(Event.current.delta,  Event.current);
                }}
            };
        }


        public void Init(Graph graph)
        {
            this.graph = graph;
            Repaint();
        }

        protected virtual void OnGUI()
        {
            var controlId = GUIUtility.GetControlID(FocusType.Passive);

            var controlEvent = Event.current.GetTypeForControl(controlId);

            if (this.EventMap.ContainsKey(controlEvent))
            {
                this.EventMap[controlEvent].Invoke();
            }

            DrawOngoingTransitionLine();
            DrawNodeWindows();
        }

        private void DrawNodeWindow(int key)
        {
            this.graph.Nodes[key].DrawWindowContent();
            GUI.DragWindow();
        }

        private void DrawNodeWindows()
        {
            if (this.graph != null)
            {

                HashSet<int> drawnEdges = new HashSet<int>();

                foreach (KeyValuePair<int, BaseNode> keyValuePair in this.graph.Nodes)
                {
                    this.DrawNodeWindowCurves(keyValuePair.Value, drawnEdges);
                    drawnEdges.Add(keyValuePair.Key);
                }

                Color defaultColor = GUI.color;
                BeginWindows();
                foreach (KeyValuePair<int, BaseNode> keyValuePair in this.graph.Nodes)
                {
                    GUI.color = keyValuePair.Value.Color;
                    keyValuePair.Value.WindowRect = GUI.Window(keyValuePair.Key, keyValuePair.Value.WindowRect, DrawNodeWindow, keyValuePair.Value.Name);
                }
                EndWindows();
                GUI.color = defaultColor;
            }
        }

        private void DrawOngoingTransitionLine()
        {
            if (transitionMode && selectedNode != null)
            {
                Rect mouseRect = new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y, 10, 10);
                DrawNodeCurve(selectedNode.WindowRect, mouseRect);

                Repaint();
            }
        }

        private void DrawNodeWindowCurves(BaseNode node, HashSet<int> drawnEdges)
        {
            foreach (KeyValuePair<int, Edge> keyValuePair in node.OutboundEdges)
            {
                if (!drawnEdges.Contains(keyValuePair.Value.NodeIdTo))
                {
                    BaseNode baseNodeTo = this.graph.FindNode(keyValuePair.Value.NodeIdTo);
                    this.DrawNodeCurve(node.WindowRect, baseNodeTo.WindowRect);
                }
            }
        }

        private void DrawNodeCurve(Rect start, Rect end)
        {
            Vector3 startPosition = new Vector3(start.x + start.width / 2, start.y + start.height / 2, 0);
            Vector3 endPosition = new Vector3(end.x + end.width / 2, end.y + end.height / 2, 0);

            Vector3 startTan = startPosition + Vector3.right * 50;
            Vector3 endTan = endPosition + Vector3.left * 50;

            Color shadowColor = new Color(0, 0, 0, .06f);

            for (int i = 0; i < 3; i++)
            {
                Handles.DrawBezier(startPosition, endPosition, startTan, endTan, shadowColor, null, (i + 1) * 5);
            }
            Handles.DrawBezier(startPosition, endPosition, startTan, endTan, Color.black, null, 1);
        }

        protected virtual void OnMouseDown(MouseButton button, Vector2 position, Event current)
        {
            if (button == MouseButton.Right)
            {
                HandleRightMouseDown(current);
            }
            else if (button == MouseButton.Left)
            {
                HandleLeftMouseDown(current);
            }
        }

        private void HandleLeftMouseDown(Event current)
        {
            bool clickedOnWindow;
            int key;
            this.IsNodeWindowClicked(current.mousePosition, out clickedOnWindow, out key);

            if (transitionMode)
            {
                if (clickedOnWindow && !this.graph.Nodes[key].Equals(selectedNode))
                {
                    // insert edges from both sides since it is undericted graph
                    this.graph.InsertEdge(selectedNode.Id, this.graph.Nodes[key].Id);
                    this.graph.InsertEdge(this.graph.Nodes[key].Id, selectedNode.Id);
                }

                transitionMode = false;
                selectedNode = null;
                current.Use();
            }
            else if (clickedOnWindow)
            {
                this.selectedNode = this.graph.Nodes[key];
            }
            else
            {
                this.selectedNode = null;
            }
        }

        private void HandleRightMouseDown(Event current)
        {
            if (isEditModeEnabled)
            {
                bool clickedOnWindow;
                int key;
                this.IsNodeWindowClicked(current.mousePosition, out clickedOnWindow, out key);

                GenericMenu menu = new GenericMenu();
                if (!clickedOnWindow)
                {
                    if (isNonTerminalNodeOptionEnabled)
                    {
                        menu.AddItem(new GUIContent("Add Nonterminal Node"), false, () => { AddNonterminalNode(current); });
                    }

                    if (isTerminalNodeOptionEnabled)
                    {
                        menu.AddItem(new GUIContent("Add Terminal Node"), false, () => { AddTerminalNode(current); });
                    }

                    if (isJokerNodeOptionEnabled)
                    {
                        menu.AddItem(new GUIContent("Add Joker Node"), false, () => { AddJokerNode(current); });
                    }
                }
                else
                {
                    menu.AddItem(new GUIContent("Make Transition"), false, () => { StartTransitionMode(key); });
                    menu.AddSeparator("");
                    menu.AddItem(new GUIContent("Delete Node"), false, () => { DeleteNodeWindow(key); });
                }
                current.Use();
                menu.ShowAsContext();
            }
        }


        internal void AddNonterminalNode(Event current)
        {
            this.graph.InsertNode<NonterminalNode>("", new Rectangle(current.mousePosition.x, current.mousePosition.y, BaseNode.DefaultWidth, BaseNode.DefaultHeight));
        }

        internal void AddTerminalNode(Event current)
        {
            this.graph.InsertNode<TerminalNode>("", new Rectangle(current.mousePosition.x, current.mousePosition.y, BaseNode.DefaultWidth, BaseNode.DefaultHeight));
        }

        internal void AddJokerNode(Event current)
        {
            this.graph.InsertNode<JokerNode>("", new Rectangle(current.mousePosition.x, current.mousePosition.y, BaseNode.DefaultWidth, BaseNode.DefaultHeight));
        }

        internal void StartTransitionMode(int key)
        {
            selectedNode = this.graph.Nodes[key];
            transitionMode = true;
        }

        internal void DeleteNodeWindow(int key)
        {
            BaseNode selectedNode = this.graph.Nodes[key];
            this.graph.DeleteNode(selectedNode.Id);
        }

        private void IsNodeWindowClicked(Vector2 mousePostion, out bool clickedOnWindow, out int key)
        {
            key = -1;
            clickedOnWindow = false;

            foreach (KeyValuePair<int, BaseNode> keyValuePair in this.graph.Nodes)
            {
                if (keyValuePair.Value.WindowRect.Contains(mousePostion))
                {
                    key = keyValuePair.Key;
                    clickedOnWindow = true;
                }
            }
        }

        protected void OnKeyDown(Keyboard keyboard)
        {
        }

        protected void OnKeyUp(Keyboard keyboard)
        {
        }

        protected virtual void OnMouseDrag(MouseButton button, Vector2 position, Vector2 delta, Event current)
        {
        }

        protected virtual void OnMouseMove(Vector2 position, Vector2 delta, Event current)
        {
        }

        protected virtual void OnContext()
        {
        }

        protected virtual void OnLayout()
        {
        }

        protected virtual void OnRepaint()
        {
        }

        protected virtual void OnScrollWheel(Vector2 delta, Event current)
        {
        }

    }
}