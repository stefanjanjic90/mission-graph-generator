﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MissionGraphGeneratorPlugin.Common.Rule;
using MissionGraphGeneratorPlugin.Data;
using MissionGraphGeneratorPlugin.RuleEditor;

namespace MissionGraphGeneratorPlugin.RuleExplorer
{
    public class RuleExplorerWindow : EditorWindow
    {
        private Vector2 graphExplorerScrollPosition;
        private List<string> ruleRemovalList = new List<string>();

        public RuleExplorerWindow()
        {
            ApplicationData.Instance.OnRuleMapChange(this.OnRuleMapChangeHanlder);
        }

        protected virtual void OnGUI()
        {
            EditorGUILayout.BeginVertical();

            this.graphExplorerScrollPosition = EditorGUILayout.BeginScrollView(this.graphExplorerScrollPosition);
            EditorGUILayout.Space();

            foreach (KeyValuePair<string, Rule> entry in ApplicationData.Instance.RuleMap)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(entry.Value.Name);
                EditorGUILayout.Space();

                if (GUILayout.Button("Edit", EditorStyles.miniButtonRight))
                {
                    this.EditRule(entry.Key);
                }

                if (GUILayout.Button("Remove", EditorStyles.miniButtonRight))
                {
                    this.ruleRemovalList.Add(entry.Key);
                }

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("Add new Rule"))
            {
                this.AddRule();
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            this.RemoveRules();

        }

        private void AddRule() { RuleEditorWindow.ShowWindow(); }

        private void EditRule(string ruleKey) { RuleEditorWindow.ShowWindow(ruleKey); }

        private void OnRuleMapChangeHanlder(Rule rule) { this.Repaint(); }

        private void RemoveRules()
        {
            if (this.ruleRemovalList.Count > 0)
            {
                foreach (string ruleKey in this.ruleRemovalList)
                {
                    ApplicationData.Instance.RemoveRule(ruleKey);
                }
            }

            this.ruleRemovalList.Clear();
        }
    }
}
