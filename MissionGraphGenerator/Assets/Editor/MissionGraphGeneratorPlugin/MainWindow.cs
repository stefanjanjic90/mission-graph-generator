﻿using UnityEditor;
using UnityEngine;
using MissionGraphGeneratorPlugin.GraphEditor;
using MissionGraphGeneratorPlugin.RuleExplorer;
using MissionGraphGeneratorPlugin.Data;
using MissionGraphGeneratorPlugin.DPO;
using MissionGraphGeneratorPlugin.Utility;
using MissionGraphGeneratorPlugin.Common;
using MissionGraphGeneratorPlugin.RuleEditor;
using System;
using System.Collections.Generic;
using System.Text;

public class MainWindow : EditorWindow
{
    private GraphEditorWindow graphResultWindow;
    private RuleExplorerWindow ruleExplorerWindow;
    private GraphEditorWindow startGraphEditorWindow;
    private bool isUILoaded = false;

    private string[] isomporhismOptions;

    protected MainWindow()
    {
        this.isomporhismOptions = new string[] { "First", "All" };
    }

    [MenuItem("Tools/Mission Graph Generator")]
    public static void ShowWindow()
    {
        ApplicationData.Instance.Load();
        MainWindow mainWindow = EditorWindow.GetWindow<MainWindow>("MGG", true);
        Rect mainWindowPosition = new Rect()
        {
            x = 0,
            y = 0
        };
        mainWindow.position = mainWindowPosition;
    }

    public virtual void OnDestroy()
    {
        this.CloseSubWindows();
    }

    protected virtual void OnGUI()
    {
        this.UILoad();
    }

    private void UILoad()
    {
        if (!this.isUILoaded)
        {
            this.isUILoaded = true;
            this.CreateSubWindows();
            this.SetWindowSizes();
            this.DockSubWindows();
        }

        EditorGUILayout.BeginVertical();

        EditorGUILayout.LabelField("Properties");
        ApplicationData.Instance.NumberOfIterations = EditorGUILayout.IntField("Number of Iterations", ApplicationData.Instance.NumberOfIterations);
        ApplicationData.Instance.EnableRuleProbability = EditorGUILayout.Toggle("Enable Rule Probability", ApplicationData.Instance.EnableRuleProbability);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Isomorphisms");
        ApplicationData.Instance.IsomorphismOption = GUILayout.Toolbar(ApplicationData.Instance.IsomorphismOption, this.isomporhismOptions);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Start Graph");
        if (GUILayout.Button("Configure Start Graph"))
        {
            this.OpenStartGraphEditorWindow();
        }
        EditorGUILayout.Space();

        if (GUILayout.Button("Start Execution"))
        {

            RuleMapValidator ruleMapValidator = RuleMapValidator.GetValidator();
            ruleMapValidator.ValidateRuleMapProbability = ApplicationData.Instance.EnableRuleProbability;
            RuleMapErrors ruleMapErrors = ruleMapValidator.Validate(ApplicationData.Instance.RuleMap);
            if (ruleMapErrors.HasErrors())
            {
                this.DispalayRuleMapErrors(ruleMapErrors);
            }
            else
            {
                Graph startGraph = DeepCloner.Clone<Graph>(ApplicationData.Instance.StartGraph);
                DPOController dpoController = new DPOController(
                    startGraph,
                    ApplicationData.Instance.RuleMap,
                    ApplicationData.Instance.NumberOfIterations,
                    ApplicationData.Instance.EnableRuleProbability,
                    ApplicationData.Instance.IsomorphismOption);
                dpoController.Execute();
                this.graphResultWindow.Graph = dpoController.ResultGraph;
            }

        }

        EditorGUILayout.EndVertical();
    }

    private void DispalayRuleMapErrors(RuleMapErrors ruleMapErrors)
    {
        StringBuilder errorString = new StringBuilder("Rules are invalid due to following problems:\n");

        int errorNumber = 1;
        foreach (KeyValuePair<RuleMapErrorCode, string> entry in ruleMapErrors.Errors)
        {
            errorString.Append(errorNumber + ". " + entry.Value);
            errorString.Append("\n");
            errorNumber++;
        }

        EditorUtility.DisplayDialog("Rules Invalid", errorString.ToString(), "OK");
    }

    private void OpenStartGraphEditorWindow()
    {
        StartGraphEditorWindow.ShowWindow();
    }

    private void CreateSubWindows()
    {
        // result window
        this.graphResultWindow = ScriptableObject.CreateInstance<GraphEditorWindow>();
        this.graphResultWindow.titleContent = new GUIContent("Result");
        this.graphResultWindow.IsEditModeEnabled = false;
        this.graphResultWindow.Show();

        // rule explorer window
        this.ruleExplorerWindow = ScriptableObject.CreateInstance<RuleExplorerWindow>();
        this.ruleExplorerWindow.titleContent = new GUIContent("Rule Explorer");
        Debug.LogWarning(Screen.currentResolution);
        this.ruleExplorerWindow.Show();
    }

    private void DockSubWindows()
    {
        Docker.Dock(this, this.graphResultWindow, Docker.DockPosition.Right);
        Docker.Dock(this, this.ruleExplorerWindow, Docker.DockPosition.Bottom);
    }

    private void SetWindowSizes()
    {
        int widthMarginPx = 10;
        int heightMarginPx = 100;

        this.minSize = new Vector2(Screen.currentResolution.width / 5 - widthMarginPx, Screen.currentResolution.height * 2 / 5 - heightMarginPx);
        this.graphResultWindow.minSize = new Vector2(Screen.currentResolution.width * 4 / 5 - widthMarginPx, Screen.currentResolution.height - heightMarginPx);
        this.ruleExplorerWindow.minSize = new Vector2(Screen.currentResolution.width / 5 - widthMarginPx, Screen.currentResolution.height * 3 / 5 - heightMarginPx);
    }

    private void CloseSubWindows()
    {
        this.graphResultWindow.Close();
        this.ruleExplorerWindow.Close();
    }
}
