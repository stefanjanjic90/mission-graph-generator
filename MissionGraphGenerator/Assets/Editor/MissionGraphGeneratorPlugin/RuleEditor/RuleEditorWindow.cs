﻿using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;
using MissionGraphGeneratorPlugin.Utility;
using MissionGraphGeneratorPlugin.Common.Rule;
using MissionGraphGeneratorPlugin.Data;
using MissionGraphGeneratorPlugin.GraphEditor;
using MissionGraphGeneratorPlugin.Common.Node;

namespace MissionGraphGeneratorPlugin.RuleEditor
{
    class RuleEditorWindow : EditorWindow
    {
        private Rule oldRule;
        private Rule rule;
        private GraphEditorWindow lhsGraphEditorWindow;
        private GraphEditorWindow rhsGraphEditorWindow;

        private List<NodeMapping> nodeMappingRemovalList = new List<NodeMapping>();

        private bool isUILoaded = false;

        public static void ShowWindow(string ruleKey = "")
        {
            RuleEditorWindow ruleEditorWindow = EditorWindow.GetWindow<RuleEditorWindow>("Rule Editor");
            Rect ruleEditorWindowPosition = new Rect() { x = 0, y = 0 };
            ruleEditorWindow.position = ruleEditorWindowPosition;

            if (!string.IsNullOrEmpty(ruleKey))
            {
                ruleEditorWindow.oldRule = ApplicationData.Instance.RuleMap[ruleKey];
                ruleEditorWindow.rule = DeepCloner.Clone<Rule>(ApplicationData.Instance.RuleMap[ruleKey]);
            }
            else
            {
                ruleEditorWindow.rule = new Rule();
            }
        }

        public virtual void OnDestroy() { this.CloseSubWinows(); }

        protected virtual void OnGUI()
        {
            this.UILoad();

            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Rule Name");
            this.rule.Name = EditorGUILayout.TextField(this.rule.Name);
            EditorGUILayout.Space();

            if (ApplicationData.Instance.EnableRuleProbability)
            {
                EditorGUILayout.LabelField("Rule Probability");
                this.rule.Probability = EditorGUILayout.FloatField(this.rule.Probability);
                EditorGUILayout.Space();
            }

            EditorGUILayout.LabelField("Node Mappings");
            foreach (NodeMapping nodeMapping in this.rule.NodeMappings)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(
                    nodeMapping.LhsNode.Id + " " + nodeMapping.LhsNode.Name
                    + "-" + nodeMapping.RhsNode.Id + " " + nodeMapping.RhsNode.Name);

                if (GUILayout.Button("Remove", EditorStyles.miniButtonRight))
                {
                    this.nodeMappingRemovalList.Add(nodeMapping);
                }

                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button("Add Node Mapping"))
            {
                this.AddNodeMapping();
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Save"))
            {
                this.SaveRule();
            }
            if (GUILayout.Button("Cancel"))
            {
                this.Close();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();

            this.RemoveNodeMappings();
        }


        private void UILoad()
        {
            if (!this.isUILoaded)
            {
                this.isUILoaded = true;
                this.CreateSubWindows();
                this.SetWindowSizes();
                this.DockSubWindows();
            }
        }

        private void SaveRule()
        {
            RuleValidator ruleValidator = RuleValidator.GetValidator();

            RuleErrors ruleErrors = ruleValidator.Validate(this.rule);

            if (ruleErrors.HasErrors())
            {
                this.DisplayErrorDialog(ruleErrors);
                return;
            }

            if (this.oldRule != null && string.Compare(this.oldRule.Name, this.rule.Name, false) != 0)
            {
                ApplicationData.Instance.RemoveRule(this.oldRule.Name);
            }

            ApplicationData.Instance.SaveRule(this.rule.Name, this.rule);
            this.Close();
        }

        private void AddNodeMapping()
        {
            BaseNode lhsNode = this.lhsGraphEditorWindow.SelectedNode;
            BaseNode rhsNode = this.rhsGraphEditorWindow.SelectedNode;
            if (lhsNode != null && rhsNode != null)
            {
                if (this.rule.IsNodeMapped(lhsNode, RuleSide.Left))
                {
                    this.DisplayAddNodeMappingErrorDialog("Selected left hand node already mapped.");
                    return;
                }

                if (this.rule.IsNodeMapped(rhsNode, RuleSide.Right))
                {
                    this.DisplayAddNodeMappingErrorDialog("Selected right hand node already mapped.");
                    return;
                }

                this.rule.AddNodeMapping(lhsNode, rhsNode);
            }
            else
            {
                this.DisplayAddNodeMappingErrorDialog("Please select both left and right side node.");
            }
        }

        private void RemoveNodeMappings()
        {
            foreach (NodeMapping nodeMapping in this.nodeMappingRemovalList)
            {
                this.rule.RemoveNodeMapping(nodeMapping);

            }
        }

        private void DisplayAddNodeMappingErrorDialog(string message)
        {
            StringBuilder errorString = new StringBuilder("Cannot add rule mapping.\n");
            errorString.Append(message);

            EditorUtility.DisplayDialog("Node Mapping Invalid", errorString.ToString(), "OK");
        }

        private void DisplayErrorDialog(RuleErrors ruleErrors)
        {
            StringBuilder errorString = new StringBuilder("Rule is invalid due to following problems:\n");

            int errorNumber = 1;
            foreach (KeyValuePair<RuleErrorCode, string> entry in ruleErrors.Errors)
            {
                errorString.Append(errorNumber + ". " + entry.Value);
                errorString.Append("\n");
                errorNumber++;
            }

            EditorUtility.DisplayDialog("Rule Invalid", errorString.ToString(), "OK");
        }

        private void CreateSubWindows()
        {
            // result window
            this.lhsGraphEditorWindow = ScriptableObject.CreateInstance<GraphEditorWindow>();
            this.lhsGraphEditorWindow.titleContent = new GUIContent("Left Hand Side");
            this.lhsGraphEditorWindow.Init(this.rule.LeftHandSide);
            this.lhsGraphEditorWindow.Show();

            // rule explorer window
            this.rhsGraphEditorWindow = ScriptableObject.CreateInstance<GraphEditorWindow>();
            this.rhsGraphEditorWindow.titleContent = new GUIContent("Right Hand Side");
            this.rhsGraphEditorWindow.Init(this.rule.RightHandSide);
            this.rhsGraphEditorWindow.Show();
        }

        private void DockSubWindows()
        {
            Docker.Dock(this, this.rhsGraphEditorWindow, Docker.DockPosition.Right);
            Docker.Dock(this, this.lhsGraphEditorWindow, Docker.DockPosition.Right);
        }

        private void SetWindowSizes()
        {
            int widthMarginPx = 10;
            int heightMarginPx = 100;

            this.minSize = new Vector2(Screen.currentResolution.width * 1.5f / 8 - widthMarginPx, Screen.currentResolution.height * 4 / 5 - heightMarginPx);
            this.lhsGraphEditorWindow.minSize = new Vector2(Screen.currentResolution.width * 3.0f / 8 - widthMarginPx, Screen.currentResolution.height * 4 / 5 - heightMarginPx);
            this.rhsGraphEditorWindow.minSize = new Vector2(Screen.currentResolution.width * 3.0f / 8 - widthMarginPx, Screen.currentResolution.height * 4 / 5 - heightMarginPx);
        }

        private void CloseSubWinows()
        {
            this.lhsGraphEditorWindow.Close();
            this.rhsGraphEditorWindow.Close();
        }
    }
}
