﻿using System.Collections.Generic;
using MissionGraphGeneratorPlugin.Common.Rule;

namespace MissionGraphGeneratorPlugin.RuleEditor
{
    class RuleMapValidator
    {

        private bool validateRuleMapProbability;

        public bool ValidateRuleMapProbability { get { return this.validateRuleMapProbability; } set { this.validateRuleMapProbability = value; } }

        private RuleMapValidator()
        {
            validateRuleMapProbability = false;
        }

        public static RuleMapValidator GetValidator()
        {
            return new RuleMapValidator();
        }

        public RuleMapErrors Validate(Dictionary<string, Rule> ruleMap)
        {
            RuleMapErrors ruleMapErrors = new RuleMapErrors();

            if (validateRuleMapProbability)
            {
                this.ValidateRuleProbabilitySum(ruleMap, ruleMapErrors);
            }

            return ruleMapErrors;
        }

        private void ValidateRuleProbabilitySum(Dictionary<string, Rule> ruleMap, RuleMapErrors ruleMapErrors)
        {
            float probabilitySum = 0;

            foreach (KeyValuePair<string, Rule> entry in ruleMap)
            {
                probabilitySum += entry.Value.Probability;
            }

            if (probabilitySum != 1.0)
            {
                ruleMapErrors.AddError(RuleMapErrorCode.ProbabilitySumInvalid);
            }
        }
    }

    class RuleMapErrors
    {
        private Dictionary<RuleMapErrorCode, string> errors;

        public Dictionary<RuleMapErrorCode, string> Errors { get { return this.errors; } }

        public RuleMapErrors()
        {
            this.errors = new Dictionary<RuleMapErrorCode, string>();
        }

        public void AddError(RuleMapErrorCode ruleMapErrorCode)
        {
            this.errors.Add(ruleMapErrorCode, ruleMapErrorCode.Message());
        }

        public bool HasErrors()
        {
            return this.errors.Count > 0;
        }
    }

    public enum RuleMapErrorCode
    {
        ProbabilitySumInvalid
    }

    public static class RuleMapErrorCodeExtension
    {
        public static string Message(this RuleMapErrorCode ruleMapErrorCode)
        {
            switch (ruleMapErrorCode)
            {
                case RuleMapErrorCode.ProbabilitySumInvalid:
                    return "The probability sum of defined rules must be equal to one.";
                default:
                    return "Unknow error.";
            }
        }
    }
}
