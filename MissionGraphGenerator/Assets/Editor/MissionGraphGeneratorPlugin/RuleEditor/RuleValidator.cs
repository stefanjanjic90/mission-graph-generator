﻿using System.Collections.Generic;
using MissionGraphGeneratorPlugin.Common;
using MissionGraphGeneratorPlugin.Common.Rule;
using MissionGraphGeneratorPlugin.Common.Node;
using MissionGraphGeneratorPlugin.Utility;
using System;

namespace MissionGraphGeneratorPlugin.RuleEditor
{
    class RuleValidator
    {
        private RuleValidator()
        {
        }

        public static RuleValidator GetValidator()
        {
            return new RuleValidator();
        }

        public RuleErrors Validate(Rule rule)
        {
            RuleErrors ruleErrors = new RuleErrors();

            this.ValidateEmptyRule(rule, ruleErrors);
            this.ValidateRuleName(rule, ruleErrors);
            this.ValidateRuleProbability(rule, ruleErrors);
            this.ValidateNodePermanency(rule, ruleErrors, NodeType.TerminalNode);
            this.ValidateNodePermanency(rule, ruleErrors, NodeType.JokerNode);

            return ruleErrors;
        }

        private void ValidateRuleName(Rule rule, RuleErrors ruleErrors)
        {
            if (string.IsNullOrEmpty(rule.Name))
            {
                ruleErrors.AddError(RuleErrorCode.NameUndefined);
            }
        }

        private void ValidateEmptyRule(Rule rule, RuleErrors ruleErrors)
        {
            if (rule == null || rule.LeftHandSide == null
                || rule.LeftHandSide.Nodes == null || rule.LeftHandSide.Nodes.Count == 0)
            {
                ruleErrors.AddError(RuleErrorCode.EmptyRule);
            }
        }

        private void ValidateRuleProbability(Rule rule, RuleErrors ruleErrors)
        {
            if (rule.Probability < 0 || rule.Probability > 1)
            {
                ruleErrors.AddError(RuleErrorCode.ProbabilityValueInvalid);
            }
        }

        private void ValidateNodePermanency(Rule rule, RuleErrors ruleErrors, NodeType nodeType)
        {
            Graph lhsGraph = rule.LeftHandSide;
            Graph rhsGraph = DeepCloner.Clone<Graph>(rule.RightHandSide);

            foreach (KeyValuePair<int, BaseNode> keyValuePairLHS in lhsGraph.Nodes)
            {
                bool nodeExists = false;
                if (keyValuePairLHS.Value.GetNodeType() == nodeType)
                {
                    int nodeToDeleteId = -1;
                    foreach (KeyValuePair<int, BaseNode> keyValuePairRHS in rhsGraph.Nodes)
                    {
                        bool attributesEqual = string.Compare(keyValuePairRHS.Value.Attribute, keyValuePairLHS.Value.Attribute, false) == 0;
                        if (keyValuePairRHS.Value.GetNodeType() == nodeType && attributesEqual)
                        {
                            nodeToDeleteId = keyValuePairRHS.Value.Id;
                            nodeExists = true;
                            break;
                        }
                    }

                    if (!nodeExists)
                    {
                        switch (nodeType)
                        {
                            case NodeType.TerminalNode:
                                ruleErrors.AddError(RuleErrorCode.TerminalNodeRemoved);
                                break;
                            case NodeType.JokerNode:
                                ruleErrors.AddError(RuleErrorCode.JokerNodeRemoved);
                                break;
                            default:
                                throw new SystemException("Node permanency validation - unsupported node type.");
                        }
                        return;
                    }
                    else
                    {
                        rhsGraph.DeleteNode(nodeToDeleteId);
                    }
                }
            }
        }
    }

    class RuleErrors
    {
        private Dictionary<RuleErrorCode, string> errors;

        public Dictionary<RuleErrorCode, string> Errors { get { return this.errors; } }

        public RuleErrors()
        {
            this.errors = new Dictionary<RuleErrorCode, string>();
        }

        public void AddError(RuleErrorCode ruleErrorCode)
        {
            this.errors.Add(ruleErrorCode, ruleErrorCode.Message());
        }

        public bool HasErrors()
        {
            return this.errors.Count > 0;
        }
    }

    public enum RuleErrorCode
    {
        EmptyRule,
        NameUndefined,
        ProbabilityValueInvalid,
        TerminalNodeRemoved,
        JokerNodeRemoved
    }

    public static class RuleErrorCodeExtension
    {
        public static string Message(this RuleErrorCode ruleErrorCode)
        {
            switch (ruleErrorCode)
            {
                case RuleErrorCode.EmptyRule:
                    return "Rule is empty. Rule must have at least left hand side graph defined.";
                case RuleErrorCode.NameUndefined:
                    return "Rule name not defined.";
                case RuleErrorCode.TerminalNodeRemoved:
                    return "Cannot remove Terminal Node on the right hand side of the rule.";
                case RuleErrorCode.JokerNodeRemoved:
                    return "Cannot remove Joker Node on the right hand side of the rule.";
                case RuleErrorCode.ProbabilityValueInvalid:
                    return "Rule probability value has to be between zero and one.";
                default:
                    return "Unknow error.";
            }
        }
    }

}
