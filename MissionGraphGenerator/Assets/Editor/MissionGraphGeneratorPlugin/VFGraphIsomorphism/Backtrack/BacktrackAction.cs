﻿using MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.Backtrack
{
    enum Action
    {
        deleteMatch,
        groupMove
    }

    class BacktrackAction
    {

        Action action;
        private int graphId;
        private int nodeIndex;
        private VFGroup restoreGroup;

        public BacktrackAction(Action action, int graphId, int nodeIndex, VFGroup restoreGroup)
        {
            this.action = action;
            this.graphId = graphId;
            this.nodeIndex = nodeIndex;
            this.restoreGroup = restoreGroup;
        }

        public BacktrackAction(Action action, int graphId, int nodeIndex) : this(action, graphId, nodeIndex, (VFGroup)0) { }

        public void Backtrack(VFState vfState)
        {
            switch (this.action)
            {
                case Action.deleteMatch:
                    vfState.RemoveFromMappingList(graphId, nodeIndex);
                    break;

                case Action.groupMove:
                    vfState.MakeMove(graphId, nodeIndex, restoreGroup);
                    break;
            }
        }

    }
}
