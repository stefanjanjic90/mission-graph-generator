﻿using System.Collections.Generic;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.Backtrack
{
    class BacktrackRecord
    {
        List<BacktrackAction> backtrackActionList = new List<BacktrackAction>();

        public void AddAction(BacktrackAction backtrackAction)
        {
            backtrackActionList.Insert(0, backtrackAction);
        }

        public void SetMatch(int nodeIndex1, int nodeIndex2, VFState vfState)
        {
            this.MoveToGroup(1, nodeIndex1, VFGroup.ContainedInMapping, vfState);
            this.MoveToGroup(2, nodeIndex2, VFGroup.ContainedInMapping, vfState);

            vfState.SetMapping(nodeIndex1, nodeIndex2);

            // Add actions to undo this act...
            this.AddAction(new BacktrackAction(Action.deleteMatch, 1, nodeIndex1));
            this.AddAction(new BacktrackAction(Action.deleteMatch, 2, nodeIndex2));
        }

        public void MoveToGroup(int graphId, int nodeIndex, VFGroup group, VFState vfState)
        {
            VFGraph vfGraph = graphId == 1 ? vfState.VfGraph1 : vfState.VfGraph2;
            VFGroup oldGroup = vfGraph.GetNodeGroup(nodeIndex);

            if (oldGroup == VFGroup.FromMapping && group == VFGroup.ToMapping ||
                oldGroup == VFGroup.ToMapping && group == VFGroup.FromMapping)
            {
                group = VFGroup.FromMapping | VFGroup.ToMapping;
            }
            if (oldGroup != (oldGroup | group))
            {
                AddAction(new BacktrackAction(Action.groupMove, graphId, nodeIndex, oldGroup));
                vfState.MakeMove(graphId, nodeIndex, group);
            }
        }

        public void Backtrack(VFState vfState)
        {
            foreach (BacktrackAction action in backtrackActionList)
            {
                action.Backtrack(vfState);
            }
        }

    }
}
