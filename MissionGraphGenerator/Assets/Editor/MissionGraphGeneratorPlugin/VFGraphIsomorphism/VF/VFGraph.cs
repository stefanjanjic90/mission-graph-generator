﻿using System;
using System.Collections.Generic;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF
{
    class VFGraph
    {
        VFNode[] vfNodes;

        public int NodeCount { get { return vfNodes.Length; } }
        public int OutNodeDegree(int nodeIndex) { return vfNodes[nodeIndex].OutEdgeDegree; }
        public int InNodeDegree(int nodeIndex) { return vfNodes[nodeIndex].InEdgeDegree; }
        public int TotalNodeDegree(int nodeIndex) { return OutNodeDegree(nodeIndex) + InNodeDegree(nodeIndex); }
        public List<int> OutNodeNeighbors(int nodeIndex) { return vfNodes[nodeIndex].OutNeighbours; }
        public List<int> InNodeNeighbors(int nodeIndex) { return vfNodes[nodeIndex].InNeighbours; }
        public VFGroup GetNodeGroup(int nodeIndex) { return vfNodes[nodeIndex].Groups; }
        public void SetNodeGroup(int nodeIndex, VFGroup groups) { vfNodes[nodeIndex].Groups = groups; }
        public string GetNodeAttribute(int nodeIndex){ return vfNodes[nodeIndex].Attribute;}
        public bool IsJoker(int nodeIndex) { return vfNodes[nodeIndex].Joker; }

        public static int[] ReversePermutation(int[] permutation)
        {
            int[] permutationOut = new int[permutation.Length];
            for (int i = 0; i < permutation.Length; i++)
            {
                permutationOut[i] = Array.IndexOf<int>(permutation, i);
            }
            return permutationOut;
        }

        public VFGraph(IGraph graph, int[] nodeIndexVFGraphToGraphMap)
        {
            vfNodes = new VFNode[graph.NodeCount];
            int[] nodeIndexGraphToVFGraphMap = ReversePermutation(nodeIndexVFGraphToGraphMap);
            Dictionary<VFEdge, VFEdge> edgeDictionary = new Dictionary<VFEdge, VFEdge>();

            for (int vfNodeIndex = 0; vfNodeIndex < graph.NodeCount; vfNodeIndex++)
            {
                vfNodes[vfNodeIndex] = new VFNode(graph, nodeIndexVFGraphToGraphMap[vfNodeIndex], edgeDictionary, nodeIndexGraphToVFGraphMap);
            }
        }
    }
}
