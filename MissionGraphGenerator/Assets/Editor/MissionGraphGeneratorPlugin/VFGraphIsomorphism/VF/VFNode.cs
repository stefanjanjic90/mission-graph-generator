﻿using System.Collections.Generic;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF
{
    class VFNode
    {
        private VFEdge[] vfEdgeOutArray;
        private VFEdge[] vfEdgeInArray;
        private string attribute;
        private bool joker;
        private VFGroup groups = VFGroup.Disconnected;

        public VFNode(IGraph graph, int nodeIndex, Dictionary<VFEdge, VFEdge> edgeDictionary, int[] mpInodGraphInodVf)
        {
            int nodeId = graph.NodeIdFromNodeIndex(nodeIndex);
            this.attribute = graph.GetNodeAttribute(nodeId);
            this.joker = graph.IsJoker(nodeId);
            vfEdgeOutArray = new VFEdge[graph.OutEdgeCount(nodeId)];
            vfEdgeInArray = new VFEdge[graph.InEdgeCount(nodeId)];
            MakeEdges(graph, nodeId, edgeDictionary, mpInodGraphInodVf);
        }

        public VFGroup Groups { get { return groups; } set { groups = value; } }
        public string Attribute { get { return attribute; } }
        public bool Joker { get { return this.joker; } }
        public int InEdgeDegree { get { return vfEdgeInArray.Length; } }
        public int OutEdgeDegree { get { return vfEdgeOutArray.Length; } }

        public List<int> InNeighbours
        {
            get
            {
                List<int> inNeighbours = new List<int>(vfEdgeInArray.Length);
                foreach (VFEdge vfEdge in vfEdgeInArray)
                {
                    inNeighbours.Add(vfEdge.NodeIndexFrom);
                }
                return inNeighbours;
            }
        }

        public List<int> OutNeighbours
        {
            get
            {
                List<int> outNeighbours = new List<int>(vfEdgeOutArray.Length);
                foreach (VFEdge vfEdge in vfEdgeOutArray)
                {
                    outNeighbours.Add(vfEdge.NodeIndexTo);
                }
                return outNeighbours;
            }
        }

        public bool IsInMapping
        {
            get { return groups == VFGroup.ContainedInMapping; }
        }

        private void MakeEdges(IGraph graph, int nodeId, Dictionary<VFEdge, VFEdge> edgeDictionary, int[] nodeIndexGraphToVfGraphMap)
        {
            int nodeIndex = graph.NodeIndexFromNodeId(nodeId);
            int vfNodeIndex = nodeIndexGraphToVfGraphMap[nodeIndex];
            VFEdge vfEdgeKey = new VFEdge(0, 0, null);

            vfEdgeKey.NodeIndexFrom = vfNodeIndex;
            this.MakeOutEdges(graph, nodeId, edgeDictionary, nodeIndexGraphToVfGraphMap, ref vfEdgeKey);
            vfEdgeKey.NodeIndexTo = vfNodeIndex;
            this.MakeInEdges(graph, nodeId, edgeDictionary, nodeIndexGraphToVfGraphMap, ref vfEdgeKey);
        }

        private void MakeOutEdges(IGraph graph, int nodeId, Dictionary<VFEdge, VFEdge> edgeDictionary, int[] nodeIndexGraphToVfGraphMap, ref VFEdge vfEdgeKey)
        {
            string attribute;
            for (int i = 0; i < graph.OutEdgeCount(nodeId); i++)
            {
                vfEdgeKey.NodeIndexTo = nodeIndexGraphToVfGraphMap[graph.NodeIndexFromNodeId(graph.GetOutEdge(nodeId, i, out attribute))];

                if (!edgeDictionary.ContainsKey(vfEdgeKey))
                {
                    vfEdgeOutArray[i] = edgeDictionary[vfEdgeKey] = new VFEdge(vfEdgeKey.NodeIndexFrom, vfEdgeKey.NodeIndexTo, attribute);
                    vfEdgeKey = new VFEdge(vfEdgeKey.NodeIndexFrom, vfEdgeKey.NodeIndexTo, null);
                }
                else
                {
                    vfEdgeOutArray[i] = edgeDictionary[vfEdgeKey];
                }
            }
        }

        private void MakeInEdges(IGraph graph, int nodeId, Dictionary<VFEdge, VFEdge> edgeDictionary, int[] nodeIndexGraphToVfGraphMap, ref VFEdge vfEdgeKey)
        {
            string attribute;
            for (int i = 0; i < graph.InEdgeCount(nodeId); i++)
            {
                vfEdgeKey.NodeIndexFrom = nodeIndexGraphToVfGraphMap[graph.NodeIndexFromNodeId(graph.GetInEdge(nodeId, i, out attribute))];

                if (!edgeDictionary.ContainsKey(vfEdgeKey))
                {
                    vfEdgeInArray[i] = edgeDictionary[vfEdgeKey] = new VFEdge(vfEdgeKey.NodeIndexFrom, vfEdgeKey.NodeIndexTo, attribute);
                    vfEdgeKey = new VFEdge(vfEdgeKey.NodeIndexFrom, vfEdgeKey.NodeIndexTo, null);
                }
                else
                {
                    vfEdgeInArray[i] = edgeDictionary[vfEdgeKey];
                }
            }
        }
    }
}
