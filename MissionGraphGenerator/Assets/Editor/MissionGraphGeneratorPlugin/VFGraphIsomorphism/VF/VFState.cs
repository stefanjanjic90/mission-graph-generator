﻿using System.Collections.Generic;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.Backtrack;
using MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF
{
    class VFState
    {
        private VFGraph vfGraph1, vfGraph2;
        private const int illegalMap = -1;

        // Mapping between node positions in VfGraph and the original Graph.
        // This is just the permutation to sort the original graph nodes by degrees.
        private int[] nodeIndexVFGraphToGraphMap1;
        private int[] nodeIndexVFGraphToGraphMap2;

        // Above mappings but indexed from/to node id's from the original maps.
        private int[] nodeId1ToNodeId2Map = null;
        private int[] nodeId2ToNodeId1Map = null;

        // List of Isomorphic mappings in original maps
        private List<VFFullMapping> vfFullMappingList = null;

        // The original ILoader's - needed to map back the permutation
        // to the original node id's after the match
        private IGraph graph1, graph2;

        // The actual mappings we're building up
        private int[] nodeIndex1To2Map;
        private int[] nodeIndex2To1Map;

        private List<VFFullMapping> currentVfFullMappingList = new List<VFFullMapping>();

        // The following lists are sorted on index but since the
        // original nodes were sorted by degree, that means that
        // these are also sorted by degree which is the key.
        private SortedListNoKey<int> in1List = new SortedListNoKey<int>();
        private SortedListNoKey<int> out1List = new SortedListNoKey<int>();
        private SortedListNoKey<int> in2List = new SortedListNoKey<int>();
        private SortedListNoKey<int> out2List = new SortedListNoKey<int>();
        private SortedListNoKey<int> disconnected1List = new SortedListNoKey<int>();
        private SortedListNoKey<int> disconnected2List = new SortedListNoKey<int>();

        private int inDegreeTotal1 = 0, inDegreeTotal2 = 0, outDegreeTotal1 = 0, outDegreeTotal2 = 0;

        bool isomorphism = false;         // Subgraph Isomorphism or Full Graph isomorphism
        bool successfulMatch = false;     // Have we made a successful match?
        bool matched = false;             // Have we attempted a match?
        bool findAll = false;             // Find all subgraphs or just the first?

        private bool MassagedPermutation { get { return nodeId1ToNodeId2Map != null; } }

        private bool MassagedPermutationList { get { return vfFullMappingList != null; } }

        public int[] Mapping1To2
        {
            get
            {
                if (!successfulMatch)
                {
                    return null;
                }
                if (!MassagedPermutation)
                {
                    MassagePermutation();
                }
                return nodeId1ToNodeId2Map;
            }
        }

        public int[] Mapping2To1
        {
            get
            {
                if (!successfulMatch)
                {
                    return null;
                }
                if (!MassagedPermutation)
                {
                    MassagePermutation();
                }
                return nodeId2ToNodeId1Map;
            }
        }

        public List<VFFullMapping> Mappings
        {
            get
            {
                if (!successfulMatch)
                {
                    return null;
                }
                if (!MassagedPermutationList)
                {
                    MassagePermutationList();
                }
                return vfFullMappingList;
            }
        }

        private void MassagePermutation()
        {
            int[] nodeIndexGraphToVfGraphMap1 = VFGraph.ReversePermutation(nodeIndexVFGraphToGraphMap1);
            int[] nodeIndexGraphToVfGraphMap2 = VFGraph.ReversePermutation(nodeIndexVFGraphToGraphMap2);

            nodeId1ToNodeId2Map = new int[nodeIndex1To2Map.Length];
            nodeId2ToNodeId1Map = new int[nodeIndex2To1Map.Length];

            for (int i = 0; i < nodeIndex1To2Map.Length; i++)
            {
                int nodeIndexMapping = nodeIndex1To2Map[nodeIndexGraphToVfGraphMap1[i]];
                nodeId1ToNodeId2Map[i] = (nodeIndexMapping == illegalMap ? illegalMap : (int)graph2.NodeIdFromNodeIndex(nodeIndexVFGraphToGraphMap2[nodeIndexMapping]));
            }

            for (int i = 0; i < nodeIndex2To1Map.Length; i++)
            {
                nodeId2ToNodeId1Map[i] = graph1.NodeIdFromNodeIndex(nodeIndexVFGraphToGraphMap1[nodeIndex2To1Map[nodeIndexGraphToVfGraphMap2[i]]]);
            }
        }

        private void MassagePermutations(
            int[] nodeIndex1To2Map, int[] nodeIndex2To1Map,
            int[] nodeIndexGraphToVfGraphMap1, int[] nodeIndexGraphToVfGraphMap2,
            ref int[] nodeId1ToNodeId2Map, ref int[] nodeId2ToNodeId1Map)
        {
            nodeId1ToNodeId2Map = new int[nodeIndex1To2Map.Length];
            nodeId2ToNodeId1Map = new int[nodeIndex2To1Map.Length];

            for (int i = 0; i < nodeIndex1To2Map.Length; i++)
            {
                int nodeIndexMapping = nodeIndex1To2Map[nodeIndexGraphToVfGraphMap1[i]];
                nodeId1ToNodeId2Map[i] = (nodeIndexMapping == illegalMap ? illegalMap : (int)graph2.NodeIdFromNodeIndex(nodeIndexVFGraphToGraphMap2[nodeIndexMapping]));
            }

            for (int i = 0; i < nodeIndex2To1Map.Length; i++)
            {
                nodeId2ToNodeId1Map[i] = graph1.NodeIdFromNodeIndex(nodeIndexVFGraphToGraphMap1[nodeIndex2To1Map[nodeIndexGraphToVfGraphMap2[i]]]);
            }
        }

        public void MassagePermutationList()
        {
            int count1 = nodeIndex1To2Map.Length;
            int count2 = nodeIndex2To1Map.Length;

            // Permutations to move from VfGraph inods to Graph inods
            int[] nodeIndexGraphToVfGraphMap1 = VFGraph.ReversePermutation(nodeIndexVFGraphToGraphMap1);
            int[] nodeIndexGraphToVfGraphMap2 = VFGraph.ReversePermutation(nodeIndexVFGraphToGraphMap2);
            vfFullMappingList = new List<VFFullMapping>(currentVfFullMappingList.Count);

            foreach (VFFullMapping vfFullMapping in currentVfFullMappingList)
            {
                VFFullMapping vfFullMappingTemp = new VFFullMapping(count1, count2);
                MassagePermutations(vfFullMapping.nodeIndexMap1To2, vfFullMapping.nodeIndexMap2To1, nodeIndexGraphToVfGraphMap1, nodeIndexGraphToVfGraphMap2, ref vfFullMappingTemp.nodeIndexMap1To2, ref vfFullMappingTemp.nodeIndexMap2To1);
                vfFullMappingList.Add(vfFullMappingTemp);
            }
        }

        public VFGraph VfGraph1 { get { return this.vfGraph1; } set { this.vfGraph1 = value; } }
        public VFGraph VfGraph2 { get { return this.vfGraph2; } set { this.vfGraph2 = value; } }
        public SortedListNoKey<int> In1List { get { return this.in1List; } set { this.in1List = value; } }
        public SortedListNoKey<int> Out1List { get { return this.out1List; } set { this.out1List = value; } }
        public SortedListNoKey<int> In2List { get { return this.in2List; } set { this.in2List = value; } }
        public SortedListNoKey<int> Out2List { get { return this.out2List; } set { this.out2List = value; } }
        public SortedListNoKey<int> Disconnected1List { get { return this.disconnected1List; } set { this.disconnected1List = value; } }
        public SortedListNoKey<int> Disconnected2List { get { return this.disconnected2List; } set { this.disconnected2List = value; } }

        public delegate bool CompareDegreesDelegate(int degree1, int degree2);
        public CompareDegreesDelegate compareFunction;

        static bool CompareIsomorphism(int degree1, int degree2) { return degree1 == degree2; }
        static bool CompareSubgraphIsomorphism(int degree1, int degree2) { return degree1 >= degree2; }

        public VFState(IGraph graph1, IGraph graph2, bool isomorphism, bool findAll)
        {
            this.graph1 = graph1;
            this.graph2 = graph2;
            this.isomorphism = isomorphism;
            this.findAll = findAll;
            this.compareFunction = this.isomorphism ? new CompareDegreesDelegate(CompareIsomorphism) : new CompareDegreesDelegate(CompareSubgraphIsomorphism);

            nodeIndexVFGraphToGraphMap1 = new NodeDegreeComparer(graph1).Permutation;
            nodeIndexVFGraphToGraphMap2 = new NodeDegreeComparer(graph2).Permutation;
            vfGraph1 = new VFGraph(graph1, nodeIndexVFGraphToGraphMap1);
            vfGraph2 = new VFGraph(graph2, nodeIndexVFGraphToGraphMap2);
            nodeIndex1To2Map = new int[graph1.NodeCount];
            nodeIndex2To1Map = new int[graph2.NodeCount];
            for (int i = 0; i < graph1.NodeCount; i++)
            {
                nodeIndex1To2Map[i] = illegalMap;
                disconnected1List.Add(i);
            }
            for (int i = 0; i < graph2.NodeCount; i++)
            {
                nodeIndex2To1Map[i] = illegalMap;
                disconnected2List.Add(i);
            }
        }

        public VFState(IGraph graph1, IGraph graph2, bool isomorphism) : this(graph1, graph2, isomorphism, false) { }
        public VFState(IGraph graph1, IGraph graph2) : this(graph1, graph2, false, false) { }

        public bool Match()
        {
            if (!CompatibleDegrees())
            {
                return false;
            }

            Stack<CandidateFinder> candidateFinderStack = new Stack<CandidateFinder>();
            Stack<BacktrackRecord> backtrackRecordStack = new Stack<BacktrackRecord>();

            Match currentMatch;
            CandidateFinder candidateFinder;
            BacktrackRecord backtrackRecord;
            bool popOut = false;

            if (this.matched)
            {
                return false;
            }
            this.matched = true;

            if (IsCompleteMatch())
            {
                successfulMatch = true;
                return true;
            }

            while (true)
            {
                if (popOut)
                {
                    if (candidateFinderStack.Count <= 0)
                    {
                        break; 
                    }
                    candidateFinder = candidateFinderStack.Pop();
                    backtrackRecord = backtrackRecordStack.Pop();
                    backtrackRecord.Backtrack(this);
                }
                else
                {
                    candidateFinder = new CandidateFinder(this);
                    backtrackRecord = new BacktrackRecord();
                }
                popOut = true;
                while ((currentMatch = candidateFinder.NextCandidateMatch()) != null)
                {
                    if (IsFeasible(currentMatch))
                    {
                        if (AddMatchToSolution(currentMatch, backtrackRecord) && IsCompleteMatch())
                        {
                            successfulMatch = true;

                            if (this.findAll)
                            {
                                this.AddCurrentMatch();
                                candidateFinderStack.Push(candidateFinder);
                                backtrackRecordStack.Push(backtrackRecord);
                                break;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        candidateFinderStack.Push(candidateFinder);
                        backtrackRecordStack.Push(backtrackRecord);
                        popOut = false;
                        break;                  
                    }
                }
            }

            return successfulMatch;
        }

        private bool IsCompleteMatch() { return disconnected2List.Count == 0 && in2List.Count == 0 && out2List.Count == 0; }
        private void AddCurrentMatch() { currentVfFullMappingList.Add(new VFFullMapping(nodeIndex1To2Map, nodeIndex2To1Map)); }

        private bool CompatibleDegrees()
        {
            if (!compareFunction(vfGraph1.NodeCount, vfGraph2.NodeCount))
            {
                return false;
            }

            for (int nodeIndex = 0; nodeIndex < vfGraph2.NodeCount; nodeIndex++)
            {
                if (!compareFunction(vfGraph1.TotalNodeDegree(nodeIndex), vfGraph2.TotalNodeDegree(nodeIndex)))
                {
                    return false;
                }
            }
            return true;
        }

        public void SetMapping(int nodeIndex1, int nodeIndex2)
        {
            nodeIndex1To2Map[nodeIndex1] = nodeIndex2;
            nodeIndex2To1Map[nodeIndex2] = nodeIndex1;
        }

        public void RemoveFromMappingList(int graphIndex, int nodeIndex)
        {
            int[] nodeIndexMap = graphIndex == 1 ? nodeIndex1To2Map : nodeIndex2To1Map;
            nodeIndexMap[nodeIndex] = illegalMap;
        }

        private int GetGroupCountInList(IEnumerable<int> nodeIndexList, VFGraph vfGraph, VFGroup group)
        {
            int count = 0;

            foreach (int nodeIndex in nodeIndexList)
            {
                if (((int)vfGraph.GetNodeGroup(nodeIndex) & (int)group) != 0)
                {
                    count++;
                }
            }

            return count;
        }

        private bool IsLocallyIsomorphic(List<int> connected1List, List<int> connected2List)
        {
            int nodeCountInMapping = 0;

            foreach (int nodeIndex in connected1List)
            {
                int nodeIndexMapping = nodeIndex1To2Map[nodeIndex];
                if (nodeIndexMapping != illegalMap)
                {
                    nodeCountInMapping++;
                    if (!connected2List.Contains(nodeIndexMapping))
                    {
                        return false;
                    }
                }
            }

            if (nodeCountInMapping != GetGroupCountInList(connected2List, vfGraph2, VFGroup.ContainedInMapping))
            {
                return false;
            }
            return true;
        }

        private bool IsInOutNew(IEnumerable<int> in1List, IEnumerable<int> out1List, IEnumerable<int> in2List, IEnumerable<int> out2List)
        {
            if (!compareFunction(
                GetGroupCountInList(in1List, vfGraph1, VFGroup.FromMapping),
                GetGroupCountInList(in2List, vfGraph2, VFGroup.FromMapping)))
            {
                return false;
            }

            if (!compareFunction(
                GetGroupCountInList(out1List, vfGraph1, VFGroup.FromMapping),
                GetGroupCountInList(out2List, vfGraph2, VFGroup.FromMapping)))
            {
                return false;
            }

            if (!compareFunction(
                GetGroupCountInList(in1List, vfGraph1, VFGroup.ToMapping),
                GetGroupCountInList(in2List, vfGraph2, VFGroup.ToMapping)))
            {
                return false;
            }

            if (!compareFunction(
                GetGroupCountInList(out1List, vfGraph1, VFGroup.ToMapping),
                GetGroupCountInList(out2List, vfGraph2, VFGroup.ToMapping)))
            {
                return false;
            }

            if (!compareFunction(
                GetGroupCountInList(out1List, vfGraph1, VFGroup.Disconnected),
                GetGroupCountInList(out2List, vfGraph2, VFGroup.Disconnected)))
            {
                return false;
            }

            if (!compareFunction(
                GetGroupCountInList(in1List, vfGraph1, VFGroup.Disconnected),
                GetGroupCountInList(in2List, vfGraph2, VFGroup.Disconnected)))
            {
                return false;
            }

            return true;
        }

        private bool IsFeasible(Match match)
        {
            int nodeIndex1 = match.NodeIndex1;
            int nodeIndex2 = match.NodeIndex2;

            string node1Attribute = vfGraph1.GetNodeAttribute(nodeIndex1);
            string node2Attribute = vfGraph2.GetNodeAttribute(nodeIndex2);
            bool isNode2Joker = vfGraph2.IsJoker(nodeIndex2);

            if (string.Compare(node1Attribute, node2Attribute, false) != 0 && !isNode2Joker)
            {
                return false;
            }

            List<int> in1List = vfGraph1.InNodeNeighbors(nodeIndex1);
            List<int> in2List = vfGraph2.InNodeNeighbors(nodeIndex2);
            List<int> out1List = vfGraph1.OutNodeNeighbors(nodeIndex1);
            List<int> out2List = vfGraph2.OutNodeNeighbors(nodeIndex2);

            if (!IsLocallyIsomorphic(in1List, in2List))
            {
                return false;
            }

            if (!IsLocallyIsomorphic(out1List, out2List))
            {
                return false;
            }

            if (!IsInOutNew(out1List, in1List, out2List, in2List))
            {
                return false;
            }

            return true;
        }

        private bool AddMatchToSolution(Match match, BacktrackRecord backtrackRecord)
        {
            int nodeIndex1 = match.NodeIndex1;
            int nodeIndex2 = match.NodeIndex2;

            backtrackRecord.SetMatch(match.NodeIndex1, match.NodeIndex2, this);

            List<int> in1List = vfGraph1.InNodeNeighbors(nodeIndex1);
            List<int> in2List = vfGraph2.InNodeNeighbors(nodeIndex2);
            List<int> out1List = vfGraph1.OutNodeNeighbors(nodeIndex1);
            List<int> out2List = vfGraph2.OutNodeNeighbors(nodeIndex2);

            foreach (int nodeIndex in out1List)
            {
                if (((int)vfGraph1.GetNodeGroup(nodeIndex) & (int)(VFGroup.Disconnected | VFGroup.ToMapping)) != 0)
                {
                    backtrackRecord.MoveToGroup(1, nodeIndex, VFGroup.FromMapping, this);
                }
            }
            foreach (int nodeIndex in in1List)
            {
                if (((int)vfGraph1.GetNodeGroup(nodeIndex) & (int)(VFGroup.Disconnected | VFGroup.FromMapping)) != 0)
                {
                    backtrackRecord.MoveToGroup(1, nodeIndex, VFGroup.ToMapping, this);
                }
            }
            foreach (int nodeIndex in out2List)
            {
                if (((int)vfGraph2.GetNodeGroup(nodeIndex) & (int)(VFGroup.Disconnected | VFGroup.ToMapping)) != 0)
                {
                    backtrackRecord.MoveToGroup(2, nodeIndex, VFGroup.FromMapping, this);
                }
            }
            foreach (int nodeIndex in in2List)
            {
                if (((int)vfGraph2.GetNodeGroup(nodeIndex) & (int)(VFGroup.Disconnected | VFGroup.FromMapping)) != 0)
                {
                    backtrackRecord.MoveToGroup(2, nodeIndex, VFGroup.ToMapping, this);
                }
            }

            if (!compareFunction(outDegreeTotal1, outDegreeTotal2) ||
                !compareFunction(inDegreeTotal1, inDegreeTotal2))
            {
                return false;
            }
            return compareFunction(out1List.Count, out2List.Count) && compareFunction(in1List.Count, in2List.Count);
        }

        public void MakeMove(int graphIndex, int nodeIndex, VFGroup newGroup)
        {
            VFGraph vfGraph;
            SortedListNoKey<int> disconnectedList;

            if (graphIndex == 1)
            {
                vfGraph = VfGraph1;
                disconnectedList = Disconnected1List;
            }
            else
            {
                vfGraph = VfGraph2;
                disconnectedList = Disconnected2List;
            }

            int currentGroupCode = (int)vfGraph.GetNodeGroup(nodeIndex);
            int newGroupCode = (int)newGroup;

            int removeGroup = currentGroupCode & ~newGroupCode;
            int addGroup = newGroupCode & ~currentGroupCode;

            if (removeGroup != 0)
            {
                if ((removeGroup & (int)VFGroup.Disconnected) != 0)
                {
                    disconnectedList.Delete(nodeIndex);
                }
                if ((removeGroup & (int)VFGroup.FromMapping) != 0)
                {
                    if (graphIndex == 1)
                    {
                        out1List.Delete(nodeIndex);
                        outDegreeTotal1--;
                    }
                    else
                    {
                        out2List.Delete(nodeIndex);
                        outDegreeTotal2--;
                    }
                }
                if ((removeGroup & (int)VFGroup.ToMapping) != 0)
                {
                    if (graphIndex == 1)
                    {
                        in1List.Delete(nodeIndex);
                        inDegreeTotal1--;
                    }
                    else
                    {
                        in2List.Delete(nodeIndex);
                        inDegreeTotal2--;
                    }
                }
            }
            if (addGroup != 0)
            {
                if ((addGroup & (int)VFGroup.Disconnected) != 0)
                {
                    disconnectedList.Add(nodeIndex);
                }
                if ((addGroup & (int)VFGroup.FromMapping) != 0)
                {
                    if (graphIndex == 1)
                    {
                        out1List.Add(nodeIndex);
                        outDegreeTotal1++;
                    }
                    else
                    {
                        out2List.Add(nodeIndex);
                        outDegreeTotal2++;
                    }
                }
                if ((addGroup & (int)VFGroup.ToMapping) != 0)
                {
                    if (graphIndex == 1)
                    {
                        in1List.Add(nodeIndex);
                        inDegreeTotal1++;
                    }
                    else
                    {
                        in2List.Add(nodeIndex);
                        inDegreeTotal2++;
                    }
                }
            }
            vfGraph.SetNodeGroup(nodeIndex, newGroup);
        }
    }
}