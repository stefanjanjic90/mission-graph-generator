﻿using System;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF
{
    [Flags]
    enum VFGroup
    {
        ContainedInMapping = 1,     // Contained in the mapping
        FromMapping = 2,            // Outside the mapping but pointed to from the mapping
        ToMapping = 4,              // Outside the mapping but points to a node in the mapping
        Disconnected = 8            // Outside the mapping with no links to mapped nodes
    }
}
