﻿using System;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF
{
    class VFEdge : IEquatable<VFEdge>
    {
        private int nodeIndexFrom;
        private int nodeIndexTo;
        private string attribute;

        public int NodeIndexFrom { get { return this.nodeIndexFrom; } set { this.nodeIndexFrom = value; } }
        public int NodeIndexTo { get { return this.nodeIndexTo; } set { this.nodeIndexTo = value; } }
        public string Attribute { get { return this.attribute; } set { this.attribute = value; } }

        public VFEdge(int nodeIdFrom, int nodeIdTo, string attribute)
        {
            this.nodeIndexFrom = nodeIdFrom;
            this.nodeIndexTo = nodeIdTo;
            this.attribute = attribute;
        }

        public override int GetHashCode()
        {
            return ((this.nodeIndexFrom << 16) + nodeIndexTo).GetHashCode();
        }

        public bool Equals(VFEdge other)
        {
            return (other != null) ? (other.NodeIndexFrom.Equals(nodeIndexFrom) && other.NodeIndexTo.Equals(nodeIndexTo)) : false;
        }
    }
}
