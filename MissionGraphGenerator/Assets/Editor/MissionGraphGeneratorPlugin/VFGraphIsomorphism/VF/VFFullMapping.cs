﻿using System;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF
{
    public struct VFFullMapping
    {
        public int[] nodeIndexMap1To2;
        public int[] nodeIndexMap2To1;

        public VFFullMapping(int count1, int count2)
        {
            nodeIndexMap1To2 = new int[count1];
            nodeIndexMap2To1 = new int[count2];
        }

        public VFFullMapping(int[] nodeIndexMap1To2, int[] nodeIndexMap2To1)
        {
            int lenght = nodeIndexMap1To2.Length;
            this.nodeIndexMap1To2 = new int[lenght];
            this.nodeIndexMap2To1 = new int[lenght];
            Array.Copy(nodeIndexMap1To2, this.nodeIndexMap1To2, lenght);
            Array.Copy(nodeIndexMap2To1, this.nodeIndexMap2To1, lenght);
        }
    }
}
