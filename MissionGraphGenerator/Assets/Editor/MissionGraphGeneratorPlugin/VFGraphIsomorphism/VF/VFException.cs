﻿using System;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF
{
    class VFException : Exception
    {
        public VFException(string str) : base(str) { }
        public static void Error(string str) { throw new VFException(str); }
    }
}
