﻿namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph
{
    interface IGraph
    {
        int NodeCount { get; }
        int InEdgeCount(int nodeId);
        int OutEdgeCount(int nodeId);

        int NodeIdFromNodeIndex(int nodeIndex);
        int NodeIndexFromNodeId(int nodeId);

        int GetInEdge(int nodeId, int nodeIndexEdge, out string attribute);
        int GetOutEdge(int nodeId, int nodeIndexEdge, out string attribute);

        string GetNodeAttribute(int nodeId);
        bool IsJoker(int nodeId);
    }
}
