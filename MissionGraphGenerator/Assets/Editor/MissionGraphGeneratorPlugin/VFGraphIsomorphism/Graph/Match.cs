﻿namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph
{
    class Match
    {
        private int nodeIndex1;
        private int nodeIndex2;

        public int NodeIndex1 { get { return this.nodeIndex1; } set { this.nodeIndex1 = value; } }
        public int NodeIndex2 { get { return this.nodeIndex2; } }

        public Match(int nodeIndex1, int nodeIndex2)
        {
            this.nodeIndex1 = nodeIndex1;
            this.nodeIndex2 = nodeIndex2;
        }
    }
}
