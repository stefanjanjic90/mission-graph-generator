﻿namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph
{
    interface IContextCheck
    {
        bool Compatible(IContextCheck icc);
    }
}
