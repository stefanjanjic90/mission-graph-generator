﻿using MissionGraphGeneratorPlugin.VFGraphIsomorphism.VF;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph
{
    class CandidateFinder
    {
        private VFState vfState;
        private int[] nodeIndexGraph1Array;
        private int currentNodeIdIndex = 0;
        private Match match;
        private bool failImmediately = false;

        public CandidateFinder(VFState vfState)
        {
            this.vfState = vfState;

            if (
                !this.vfState.compareFunction(this.vfState.Out1List.Count, this.vfState.Out2List.Count) ||
                !this.vfState.compareFunction(this.vfState.In1List.Count, this.vfState.In2List.Count) ||
                !this.vfState.compareFunction(this.vfState.Disconnected1List.Count, this.vfState.Disconnected2List.Count))
            {
                this.failImmediately = true;
                return;
            }
            if (this.vfState.Out2List.Count > 0 && this.vfState.Out1List.Count > 0)
            {
                this.nodeIndexGraph1Array = new int[this.vfState.Out1List.Count];
                this.vfState.Out1List.CopyTo(this.nodeIndexGraph1Array);
                SetInitialMatch(this.vfState.Out1List[0], this.vfState.Out2List[0]);
            }
            else if (this.vfState.In2List.Count > 0 && this.vfState.In1List.Count > 0)
            {
                this.nodeIndexGraph1Array = new int[this.vfState.In1List.Count];
                this.vfState.In1List.CopyTo(this.nodeIndexGraph1Array);
                SetInitialMatch(this.vfState.In1List[0], this.vfState.In2List[0]);
            }
            else if (this.vfState.Disconnected1List.Count >= 0)
            {
                this.nodeIndexGraph1Array = new int[this.vfState.Disconnected1List.Count];
                this.vfState.Disconnected1List.CopyTo(this.nodeIndexGraph1Array);
                SetInitialMatch(this.vfState.Disconnected1List[0], this.vfState.Disconnected2List[0]);
            }
        }

        public void SetInitialMatch(int nodeIndex1, int nodeIndex2)
        {
            if (!ValidDegrees(nodeIndex1, nodeIndex2))
            {
                this.failImmediately = true;
            }
            this.match = new Match(nodeIndex1, nodeIndex2);
        }

        public bool ValidDegrees(int nodeIndex1, int nodeIndex2)
        {
            // We must always have the degrees in graph1 at least as large as those in graph2.  Also,
            // since we order the nodes by total degree size, when we fail this condition, we know that
            // there are no further nodes in graph1 which will match the current graph2 node so we can
            // abandon the search.
            int totalDegrees1 = this.vfState.VfGraph1.InNodeDegree(nodeIndex1) + this.vfState.VfGraph1.OutNodeDegree(nodeIndex1);
            int totalDegrees2 = this.vfState.VfGraph2.InNodeDegree(nodeIndex2) + this.vfState.VfGraph2.OutNodeDegree(nodeIndex2);
            return this.vfState.compareFunction(totalDegrees1, totalDegrees2);
        }

        public Match NextCandidateMatch()
        {
            if (this.failImmediately)
            {
                return null;
            }

            if (this.currentNodeIdIndex < this.nodeIndexGraph1Array.Length)
            {
                this.match.NodeIndex1 = this.nodeIndexGraph1Array[this.currentNodeIdIndex++];
                if (!ValidDegrees(this.match.NodeIndex1, this.match.NodeIndex2))
                {
                    return null;
                }
                return this.match;
            }
            return null;
        }
    }
}
