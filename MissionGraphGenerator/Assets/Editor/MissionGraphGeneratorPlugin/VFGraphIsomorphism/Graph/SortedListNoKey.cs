﻿using System.Collections.Generic;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph
{
    class SortedListNoKey<T>
    {
        SortedList<T, byte> sortedList = new SortedList<T, byte>();

        public void Add(T item) { this.sortedList.Add(item, 0); }
        public void CopyTo(T[] array) { this.sortedList.Keys.CopyTo(array, 0); }
        public int Count { get { return this.sortedList.Count; } }
        public T this[int i] { get { return this.sortedList.Keys[i]; } }
        public void Delete(T item) { this.sortedList.Remove(item); }
    }
}
