﻿using System;
using System.Collections.Generic;

namespace MissionGraphGeneratorPlugin.VFGraphIsomorphism.Graph
{
    class NodeDegreeComparer : IComparer<int>
    {
        private IGraph graph;

        public NodeDegreeComparer(IGraph graph) { this.graph = graph; }

        public int[] Permutation
        {
            get
            {
                int[] permutation = new int[graph.NodeCount];
                IComparer<int> nodeDegreeComparer = new NodeDegreeComparer(graph);

                for (int i = 0; i < graph.NodeCount; i++)
                {
                    permutation[i] = i;
                }

                Array.Sort(permutation, nodeDegreeComparer);
                return permutation;
            }
        }

        public int Compare(int x, int y)
        {
            if (x == y)
            {
                return 0;
            }
            int nodeIdX = graph.NodeIdFromNodeIndex(x);
            int nodeIdY = graph.NodeIdFromNodeIndex(y);
            int xDegree = graph.InEdgeCount(nodeIdX) + graph.OutEdgeCount(nodeIdX);
            int yDegree = graph.InEdgeCount(nodeIdY) + graph.OutEdgeCount(nodeIdY);
            return xDegree < yDegree ? 1 : -1;
        }
    }
}
