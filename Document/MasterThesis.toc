\contentsline {chapter}{\numberline {1}Uvod}{3}
\contentsline {chapter}{\numberline {2}Grafovi}{5}
\contentsline {section}{\numberline {2.1}Osnovne definicije}{5}
\contentsline {section}{\numberline {2.2}Izomorfizam grafova}{7}
\contentsline {subsection}{\numberline {2.2.1}Definicije i problem izomorfizma podgrafa}{8}
\contentsline {subsection}{\numberline {2.2.2}Algoritam VF2}{9}
\contentsline {subsubsection}{Primer izvr\IeC {\v s}avanja algoritma VF2}{10}
\contentsline {chapter}{\numberline {3}Metod dvostrukog potiskivanja}{12}
\contentsline {section}{\numberline {3.1}Uvod}{12}
\contentsline {section}{\numberline {3.2}Teorija kategorija i osnovne definicije}{12}
\contentsline {subsubsection}{Usmereni graf kao primer kategorije}{13}
\contentsline {section}{\numberline {3.3}Transformacija grafova}{13}
\contentsline {subsection}{\numberline {3.3.1}Grafovi sa labelama}{13}
\contentsline {subsection}{\numberline {3.3.2}Spoj i produkcijska pravila}{14}
\contentsline {subsubsection}{Problemati\IeC {\v c}ne situacije}{15}
\contentsline {section}{\numberline {3.4}Grafovske gramatike}{17}
\contentsline {chapter}{\numberline {4}Unity kao okru\IeC {\v z}enje za razvoj}{19}
\contentsline {section}{\numberline {4.1}Osnovne odlike}{19}
\contentsline {section}{\numberline {4.2}Pisanje dodataka za razvojno okru\IeC {\v z}enje}{20}
\contentsline {chapter}{\numberline {5}Proceduralno generisanje grafovskim gramatikama}{23}
\contentsline {section}{\numberline {5.1}Osnovni pojmovi}{23}
\contentsline {subsection}{\numberline {5.1.1}Osobine}{24}
\contentsline {subsection}{\numberline {5.1.2}Klase}{24}
\contentsline {subsection}{\numberline {5.1.3}Motivi za upotrebu proceduralnog generisanja}{25}
\contentsline {section}{\numberline {5.2}Misije i grafovske gramatike}{26}
\contentsline {subsection}{\numberline {5.2.1}Misije u video-igrama}{26}
\contentsline {subsection}{\numberline {5.2.2}Generisanje misija grafovskim gramatikama}{26}
\contentsline {chapter}{\numberline {6}Unity dodatak za generisanje misija}{29}
\contentsline {section}{\numberline {6.1}Glavni prozor}{29}
\contentsline {subsection}{\numberline {6.1.1}Konfigurisanje algoritma}{30}
\contentsline {subsubsection}{Po\IeC {\v c}etni graf}{30}
\contentsline {subsubsection}{Broj iteracija}{30}
\contentsline {subsubsection}{Prvi ili svaki izomorfizam}{31}
\contentsline {subsubsection}{Pravila sa verovatno\IeC {\'c}om}{31}
\contentsline {subsection}{\numberline {6.1.2}Manipulacija pravilima gramatike}{31}
\contentsline {subsection}{\numberline {6.1.3}Pokretanje algoritma i rezultat}{31}
\contentsline {section}{\numberline {6.2}Prozor za definisanje pravila gramatike}{31}
\contentsline {subsubsection}{Definisanje leve i desne strane pravila}{31}
\contentsline {subsubsection}{Verovatno\IeC {\'c}a}{32}
\contentsline {section}{\numberline {6.3}Renderovanje i manipulacija grafova}{32}
\contentsline {section}{\numberline {6.4}Aplikacijski podaci}{33}
\contentsline {section}{\numberline {6.5}Dvostruki potisak}{33}
\contentsline {subsection}{\numberline {6.5.1}Primeri misija dobijenih generisanjem}{35}
\contentsline {subsubsection}{Primer 1.}{35}
\contentsline {subsubsection}{Primer 2.}{36}
\contentsline {chapter}{\numberline {7}Dalji razvoj}{37}
\contentsline {chapter}{\numberline {8}Zaklju\IeC {\v c}ak}{38}
\contentsline {chapter}{Bibliografija}{39}
