\hspace{\parindent} Proceduralno generisanje je programski pristup kreiranju sadržaja za video-igre. U zavisnosti od implementacije generatora sadržaj se može kreirati potpuno automatski, korišćenjem isključivo indirektnog korisničkog unosa. Sa druge strane imamo generatore koji generišu sadržaj uz pomoć korisnika, koji je u mogućnosti da konfiguriše ograničeni skup parametara. Ovakvi generatori obično služe dizajnerima kao pomoćni alat prilikom razvoja. 

Kada pričamo o sadržaju to mogu biti različiti aspekti video-igara: mape, pravila igre, geometrija, teksture, predmeti, misije i drugi elementi. Ovde ćemo obratiti pažnju na proceduralno generiasanje misija.
Prvo uvodimo neke osnovne pojmove proceduralnog generisanja, nakon toga izlažemo kako se to grafovske gramatike koriste za generisanje misija.

\section{Osnovni pojmovi}
Pojmovi \textit{proceduralno} i \textit{generisanje} nam naglašavaju da koristimo računarske algoritme sa ciljem da kreiramo nešto.
Pod \textit{sistemima za proceduralno generisanje} podrazumevamo sisteme koji implementiraju neki ovakav algoritam. To može biti video-igra koja sama generiše neki sadržaj ili alat koji dizajner koristi prilikom svog rada.
Navešćemo neke primere sistema:
\begin{itemize}
	\item Program koji generiše mape za potrebe igranja, bez ikakvog unosa korisnika. 
	\textit{Diablo} je tipičan primer igre koji sadrži ovakav sistem.
	\item Alat za dizajniranje mapa koji kontinualno evaluira trenutni dizajn i daje predloge za unapređenja.
	\item Sistem koji generiše i populiše nivoe u igricama sa vegetacijom ili drugim statičkim objektima.
	\item Alat za generisanje i dizajniranje vegetacije. \textit{SpeedTree} je popularno rešenje za ovaj problem.
	\item Programi za proceduralno generisanje geometrije i animacije. Dobar predstavnik ovakvih sistema je \textit{Houdini FX} \cite{houdini}.
\end{itemize} 

\subsection{Osobine}
Kod sistema za proceduralno generisanje sadržaja postoje određene osobine koje mogu biti poželjne i one zavise
od samog problema generisanja koji pokušavamo da rešimo. Jedan od slučajeva može biti generisanje vegetacije u realnom vremenu,
dok drugi može biti evaluacija datog dizajna mape i davanje predloga za poboljšanje. Potrebne ili poželjne osobine se razlikuju 
u zavisnosti od primene. Opisaćemo neke od poželjnih osobina za takve sisteme:
\begin{itemize}
	\item \textit{Brzina} - Brzina generisanja zavisi od toga da li se sadržaj kreira tokom igranja
	ili tokom razvijanja igre. Prethodna dva primera baš ilustruju ove varijante.
	\item \textit{Pouzdanost} - Neki generatori su u mogućnosti da kreiraju sadržaj koji zadovoljava širok skup kriterijuma,
	ali to ne mora biti važno za sve sisteme. Ako generator napravi mapu koja nema ulaz i izlaz to se može smatrati za loše generisan
	sadržaj. Ako ipak generator vegetacije napravi drvo koje vizuelno ne izgleda privlačno, to ne mora da poremeti samu igru ili da je napravi nekorisnom.
	\item \textit{Nivo kontrole} - Određeni nivo kontrole nad generatorom je obično potreban. Tako da sam korisnik, dizajner ili igrač, može na neki način da utiče na kreiranje sadržaja. 
	Nivo kontrole obično zavisi od praktičnih potreba i primena sistema.
	Kao i kod brzine, može biti važno kada se sadržaj generiše. Kontrola se obično postiže biranjem nasumičnog semena za generisanje.
	Drugi način je izbor skupa parametara koji kontrolišu generisanje.
	\item \textit{Izražajnost i raznolikost} - Od sistema za generisanje se obično traži da mogu da generišu raznoliki sadržaj.
	Kako bismo izbegli da sadržaj izgleda isto ili slično, samo sa malim varijacijama. Merenje izražajnosti je veoma težak problem sam po sebi i dizajniranje generatora koji generiše raznoliki sadržaj bez negativnog uticaja na kvalitet takođe može biti komplikovano.
	\item \textit{Kreativnost i verodostojnost} - Najbolji slučaj bi bio da sadržaj kreiran generatorom izgleda kao da je napravljen
	od strane čoveka. U većini slučajeva to ne prolazi potpuno neprimetno.
\end{itemize} 

\subsection{Klase}
Proceduralne generatore možemo razvrstavati u više različitih klasa u zavisnosti od njihovih, već spomenutih, osobina i drugih odlika koje mogu posedovati.
\begin{itemize}
	\item \textit{Online i Offline sistemi}. Pod \textit{online} sistemima podrazumevamo generatore koji su u sposobnosti da 
	kreiraju sadržaj tokom igranja igre. Na ovaj način možemo dobiti igre koje nemaju kraj i menjaju sadržaj na osnovu 
	načina igranja. \textit{Offline} generatori se koriste tokom razvijanja video-igre ili pre početka nivoa, tokom učitavanja video-igre.
	Ovakvi sistemi se obično koriste za generisanje kompleksnih okruženja i mapa.
	
	\item {Sistemi za generisanje neophodnog i opcionog sadržaja}. Sadržaj koji je potreban za početak, razvijanje i kraj nivoa smatra se neophodnim. Sa druge strane opcioni, ili pomoćni, sadržaj  se obično može odbaciti i zameniti sa nekim drugim. Primer ovakvog sadržaja bi bila vegetacija u igri. Osnovna razlika između ova dva je što neophodni sadržaj mora uvek biti
	ispravan dok isto ne mora da važi za opcioni (kao kod prethodno napomenutog primera sa drvetom). 
	
	\item \textit{Generički i prilagodljivi sistemi}. U slučaju kada se ponašanje igrača ne uzima u obzir prilikom kreiranja sadržaja,
	tako dobijen sadržaj nazivamo \textit{generičkim sadržajem}. Suprotno tome, kada analiziramo igranje i ponašanje igrača i to koristimo kao jedan od parametara generisanja, tada dobijamo \textit{prilagodljivi sadržaj}.
	
	\item \textit{Stohastički i deterministički sistemi}. Deterministički sistemi dopuštaju kreiranje istog sadržaja ako je kao ulaz dat isti skup parametara, dok kod stohastičkih sistema ponovno kreiranje istog sadržaja nije moguće.
	
	\item \textit{Konstruktivni i generiši i testiraj sistemi}. Konstruktivni sistemi kreiraju sadržaj u jednom prolazu. Sa druge strane "generiši i testiraj" sistemi to rade tako što smenjuju faze generisanja i testiranja dobijenog rešenja. Kada dobiju rešenje koje je zadovoljavajuće, u odnosu na neke definisane kriterijume, tada se zaustavljaju.
\end{itemize} 

\subsection{Motivi za upotrebu proceduralnog generisanja}
Sad kad znamo osobine proceduralnih generatora vredi pomenuti motive za njihovu upotrebu i videćemo da ih postoji više.

Do sada nam je jasno da sadržaj dobijen nekim sistemom za generisanje može biti veoma raznovrsan ili nepredvidiv, posebno kada su u pitanju stohastički sistemi. Možda, ne tako očigledno, sistemi za generisanje mogu dovesti do novih kreativih otkrića. Preciznije,
algoritamski pristup kreiranju može generisati sadržaj koji je radikalno drugačiji od onog koji bi čovek napravio. Van domena igrica ovakav pristup se naziva \textit{evolutivni dizajn}.

Kombinovanjem metoda proceduralnog generisanja i korisničkog ponašanja mogu se napraviti igre koje menjaju svoje elemente ili pravila
u odnosu na to kako neko igra igru. Dobar primer za ovako nešto je igra \textit{Left 4 Dead} \cite{left4dead} koja menja stanje igre, broj neprijatelja i njihovu jačinu, u odnosu na ponašanje igrača.

Možda je najbitniji motiv za generisanje sadžaja ubrzanje razvoja video-igre. Cilj može biti i uklanjanje potrebe
za sadržajem napravljenim od strane čoveka. Kako ova varijanta nije najpoželjnija za ljude koji se bave pravljenjem sadržaja,
možemo reći da alati koji asistiraju u generisanju i dizajniranju sadržaja mogu biti jako poželjni. 
Korišćenjem takvih alata razvojni timovi bi bili u mogućnosti da za kraće vreme prozvedu potrebnu količinu sadržaja za jednu video-igru.
\pagebreak

\section{Misije i grafovske gramatike}
Definicije grafova i grafovskih gramatika smo videli u prethodnim odeljcima. Sada ćemo uvesti pojam misije u video-igrama
i objasnićemo kako se to grafovske gramatike koriste za njihovo generisanje.

\subsection{Misije u video-igrama}
Video-igre su obično podeljene u nekakve celine, koje linearno slede jedna za drugom. Nalik na poglavlja u knjigama.
Jedna takva celina se obično naziva \textit{nivo}. Jedan od razloga za ovakvu podelu je da igre mogu biti toliko velike da se
moraju parcijalno učitavati i izvršavati. Nivoi uglavnom imaju jasno definisan početak i kraj, kao i uslove koje treba ispuniti da bi nivo mogao da se završi. Ne tako retko, nailazimo na nivoe kojima se težina igranja povećava kako napredujemo kroz igru.

Nivo nije jedinstvena celina već se može razložiti na elemente. Preciznije, to su \textit{misija} i \textit{prostor}.
Misija predstavlja skup zadataka koje igrač može ili mora da uradi kako bi ispunio uslove za njen uspešan završetak.
Sa druge strane, prostor je okruženje sastavljeno od geometrijskih objekata, dvodimenzionalnih ili trodimenzionalnih, kroz koje 
igrač može da se kreće u cilju ispunjavanja uslova misije.
Za misije je jako bitno da pratimo redosled zadataka, njihovu uslovnost, uzročnost i posledičnost. Kod prostora moramo voditi računa o
povezanosti njegovih delova, razdaljine i razmere, kao i o postavljanju smislenih znakova koji vode igrača kroz nivo.

Kako bismo generisali smisleni nivo potrebno je obratiti pažnju na sve elemente misije i prostora, kao i uklopiti ih međusobno.
Postoje različiti načini za generisanje nivoa video-igre. Jedan takav pristup je generisanje misije na osnovu koje se naknadno generiše prostor za igru. Ovaj način generisanja je pogodan za video-igre \textit{akciono-avanturističkog} žanra ili video-igre u kojima je akcenat na naraciji, gde je bitno obezbediti jasnu povezanost u priči.
Moguće je izvršiti generisanje i u suprotnom smeru, takav pristup je obično karakterističan za video-igre strateškog tipa, jer je
potrebna konzistentna arhitektura i geometrija. Na ovaj način postoji rizik da geometrija i prostor koji su generisani ne pružaju veliki potencijal za generisanje poželjne misije. Pored toga, nivo kontrole nad generisanjem misije je obično niži u odnosu na druge pristupe. Iz tog razloga se ovakav pristup koristi samo za video-igre određenog tipa.

\subsection{Generisanje misija grafovskim gramatikama}

Da bismo generisali misiju korišćenjem grafovskih gramatika potrebno je da prvo definišemo
alfabet gramatike. Navešćemo jednostavan primer alfabeta koji se sastoji od sledećih 
čvorova:
\begin{enumerate}
	\item Početni čvor \textit{S} - početni simbol od kog gramatika generiše misiju.
	\item Ulaz \textit{u} - početna pozicija za igrača.
	\item Zadatak \textit{Z} - zadatak koji je potrebno da igrač uradi.
	\item Katanac \textit{b} - brave koje moraju da se otključaju kako bi igrač nastavio dalje.
	\item Ključ \textit{k} - ključ koji otvara određeni katanac.
	\item Protivnik \textit{p} - protivnik kog igrač mora da pobedi.
	\item Džoker \textit{$\bigstar$} - džoker čvor koji menja bilo koji drugi čvor prilikom primene pravila.
	\item Cilj \textit{c} - dolaskom do cilja igrač završava misiju.
\end{enumerate}

Kao i kod gramatika koje manipulišu stringovima i ovde imamo terminalne i neterminalne čvorove.
Neterminalne čvorove obično označavamo velikim slovima, a terminalne malim.
Različita pravila se mogu napraviti nad ovakvim alfabetom, u nastavku navodimo primer.

\begin{figure}[H]
	\centering
	\includegraphics[width=150mm]{images/Rules}
	\caption{Primer grafovske gramatike i njenih pravila prezapisivanja.}
	\label{Rules}
\end{figure}

Prvo što možemo primetiti jeste da može biti teško kontrolisati pravila jedne gramatike.
Ovako definisanim pravilima, neka se mogu primeniti više puta, kao na primer pravilo dodavanja zadatka.
Tako da broj zadataka može da se kreće od nule pa na više, zbog nedostatka definisane granice.
Jedan od načina da se uvede kontrola je odabir redosleda primene pravila i broj primenjivanja.
Ipak, drugačiji pristup se koristi u praksi gde proces generisanja posmatramo u delovima.

Prethodni primer je prilično jednostavan. Kako bismo dobili nešto kompleksnije moramo koristiti više pravila.
Osnova strategije pri kreiranju dobrih gramatika je da se proces razbije u više koraka.
Svaki od koraka posmatramo kao jedan korak u procesu dizajniranja. Jedan korak može da generiše generalnu specifikaciju misije, dok naredni koraci mogu da se bave grupama sitnijih detalja. Praktično gledano, prvi korak bi mogao da konstruiše 
nasumičan graf, ali prateći određeni skup pravila. Dok u drugom koraku, skup pravila bi mogao da rekonstruiše dobijeni graf
u nešto što ima smisla iz perspektive igre.

Naredni primer opisuje jedno od mogućih razlaganja, gde bi svaki korak mogao da koristi drugi skup pravila:
\begin{enumerate}
	\item Generišemo misiju određene veličine i nasumično biramo između katanaca, ključeva i zadataka koji popunjavaju 
	prostor između ulaza i cilja. 
	\item Obezbeđujemo da je redosled ključeva i katanaca ispoštovan, tačnije da se za svaki katanac
	prethodno može naići na ključ. Možemo i dodeliti više ključeva jednom katancu.
	\item Vršimo pomeranje katanaca, ključeva i zadataka. Ovaj korak ne mora biti neophodan. Nasumično generisan graf u prvom
	koraku može imati manje smisla iz perspektive igre. Nekoliko katanaca ili ključeva zaredom, može dovesti do toga da igra bude
	dosadna ili frustrirajuća. U ovom koraku skup pravila bi mogao da obezbedi da ne dolazi do takvih slučajeva.
\end{enumerate}


