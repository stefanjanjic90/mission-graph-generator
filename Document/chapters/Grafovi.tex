\hspace{\parindent} Grafovi, kao jedna od osnovnih struktura koja će biti korišćena, imaju veoma široku primenu u računarstvu. Igraju veliku ulogu u metodama proceduralnog generisanja, ne samo kod generisanja misija već i u drugim slučajevima. Jedan od takvih primera je generisanje prostora za igranje u video-igrama.
Ovde ćemo opisati i definisati osnovne pojmove koji će nam biti od važnosti u narednim izlaganjima. 

\section{Osnovne definicije}

\begin{definition}
	\textit{Usmereni graf} \textit{$G = (V,E)$} se sastoji od konačnog skupa čvorova V i konačnog skupa grana \textit{E}, pri čemu grane iz \textit{E} predstavljaju uređene parove \textit{$(a, b)$} čvorova iz \textit{V}.
\end{definition}

Konačni grafovi se predstavljaju dijagramima u kojima su čvorovi predstavljeni kružićima, a grane koje spajaju dva čvora, strelicama koje povezuju odgovarajuće kružiće. Strelicama je određen smer shodno definiciji uređenog para koji se nalazi u skupu \textit{E}. Ovako definisane grane nazivaju se \textit{usmerene grane} i grafovi sa takvim granama \textit{usmereni grafovi}. U \textit{usmerenom grafu} za svaka dva čvora \textit{a} i \textit{b} u skupu \textit{E} mogu postojati uređeni parovi - grane \textit{(a,b)} i \textit{(b,a)}. U većini slučajeva ako želimo da \textit{usmereni graf} razmatramo kao \textit{neusmereni} graf, dovoljno je da za svaku granu
\textit{(a,b)} koja pripada skupu \textit{E} dodamo i granu \textit{(b,a)} u skup.  Pored toga neusmereni graf možemo definisati i na sledeći način.

\begin{definition}
	\textit{Neusmereni} graf \textit{$G=(V,E)$} se sastoji od konačnog skupa čvorova \textit{V} i kolekcije grana \textit{E}, dvoelementnih podskupova skupa \textit{V}.
	 Elementi \textit{a} i \textit{b} skupa \textit{V} su povezani pomoću grane \textit{$\{a,b\}$} ako \textit{$\{a,b\} \in E$}. 
\end{definition}

Navodimo definicje susednosti čvorova kod usmerenih i neusmerenih grafova.

\begin{definition}
	Ako je par \textit{(a,b)} grana usmerenog grafa \textit{G = (V,E)}, takav da je \textit{a} početni čvor i \textit{b} završni čvor, tada se
	za čvorove \textit{a} i \textit{b} kaže da su \textit{incidentni} grani \textit{(a,b)}. Čvor \textit{a} je susedan ka čvoru \textit{b}
	i čvor \textit{b} je susedan ka čvoru \textit{a}.
\end{definition}

Na sličan način se definiše susednost čvorova kada su u pitanju \textit{neusmereni grafovi}. 

\begin{definition}
	Ako skup \textit{$\{a,b\}$} predstavlja granu grafa \textit{G}, tada se čvorovi \textit{a} i \textit{b} nazivaju \textit{krajnjim tačkama} grane \textit{$\{a,b\}$}.
	Za granu \textit{$\{a,b\}$} se takođe kaže da je incidentna čvorovima \textit{a} i \textit{b}.
	Dva čvora su \textit{susedna} ako predstavljaju krajnje tačke iste
	grane, ili ekvivalentno tome, ako su incidentni istoj grani.
\end{definition}


\begin{definition}
	Ulazni stepen čvora \textit{$v \in V$}, u oznaci \textit{$deg^{+}(v)$}, predstavlja broj grana kojma je \textit{v} završni čvor.
	Ako je \textit{$deg^{+}(v)=0$}, tada se čvor \textit{v} naziva \textit{izvorom}.
\end{definition}

\begin{definition}
	Izlazni stepen čvora \textit{$v \in V$}, u oznaci \textit{$deg^{-}(v)$}, predstavlja broj grana kojma je \textit{v} početni čvor.
	Ako je \textit{$deg^{-}(v)=0$}, tada se čvor \textit{v} naziva \textit{ponorom}.
\end{definition}

Definicije \textit{ulaznog i izlaznog} stepena, kao i naredna definicija \textit{podgrafa}, biće nam od velikog značaja prilikom definisanja i implementacije algoritma za grafovski izomorfizam.

\begin{definition}
	Usmereni graf \textit{$G^{'}=(V^{'},E^{'})$} je \textit{usmereni podgraf} usmerenog grafa \textit{$G = (V,E)$}, u oznaci \textit{$G^{'} \preceq G$} ako važi
	\textit{$V^{'} \subseteq V$} i \textit{$E^{'} \subseteq E$}.
\end{definition}

\begin{figure}[H]
	\centering
	\includegraphics[width=150mm]{images/Subgraph}
	\caption{Grafovi \textit{$H_1$}, \textit{$H_2$}, \textit{$H_3$} predstavlaju različite podgrafe grafa \textit{G}.}
\end{figure}

Navedena definicja nam govori da svaki čvor grafa \textit{$G^{'}$} je i čvor grafa \textit{G} i svaka usmerena grana grafa \textit{$G^{'}$} je usmerena grana i grafa \textit{G}.

Posmatraćemo grafove koji nemaju izolovane čvorove ili grupe povezanih čvorova gde su grupe međusobno izolovane.
Izolovanost čvora definisaćemo pomoću stepena čvora.

\begin{definition}
	Čvor kojem su \textit{$deg^{-}(v)=0$} i \textit{$deg^{+}(v)=0$} naziva se \textit{izolovanim}.
\end{definition}

Pored izolovanih čvorova mogu postojati cele grupe izolovanih, ali međusobno povezanih čvorova, koje se obično nazivaju \textit{ostrva}.
Ipak, mi ćemo u obzir uzimati samo \textit{povezane grafove}.
Iz tog razloga, prvo definišemo \textit{put} u neusmerenom i usmerenom grafu. Nakon toga, povezanost u neusmerenom grafu na osnovu koje ćemo definisati \textit{noseći} graf i \textit{povezan} usmereni graf.

\begin{figure}[H]
	\centering
	\includegraphics[width=150mm]{images/ConnectedGraphs}
	\caption{Primeri povezanosti kod neusmerenih grafova. Povezani graf \textit{N} (levo) i nepovezani graf \textit{K} (desno).}
\end{figure}

\begin{definition}
	Neka je \textit{$G=(V,E)$} neusmereni graf sa čvorovima \textit{$v_0, v_k \in V$}. \textit{Put} dužine \textit{k} od čvora \textit{$v_0$} do čvora \textit{$v_k$} predstavlja
	sekvencu čvorova \textit{$v_0v_1v_2,...,v_{k-1}v_k$}, takvu da čvorovi \textit{$v_0,v_1,...,v_k$} pripadaju \textit{V} i grana \textit{$\{v_{i},v_{i+1}\}$} pripada \textit{E} za svako \textit{i} iz \textit{$\{0,1,...,k-1\}$}.
\end{definition}

\begin{definition}
	\textit{Usmereni put} od čvora \textit{a} do čvora \textit{b} je opisan kao sekvenca čvorova \textit{$v_0v_1v_2...v_n$}, gde je \textit{$a=v_0$}, \textit{$b=v_n$}, 
	a \textit{$(v_{i},v_{i+1})$} je usmerena grana koja pripada \textit{E} za svako \textit{i} iz \textit{$\{0,1,...,k-1\}$}.
	\textit{Dužina} puta predstavlja broj usmerenih grana u putu.
\end{definition}
 
 \begin{definition}
 	Neusmereni graf \textit{$G=(V,E)$} je \textit{povezan} ako postoji \textit{put} između bilo koja dva različita čvora grafa G.
 \end{definition}
 
 Za usmereni graf \textit{G} prvo opisujemo neusmereni graf \textit{$G^{n}$}, takav da svaka usmerena grana grafa \textit{G} prelazi u neusmerenu granu grafa \textit{$G^{n}$}.
 
 \begin{definition}
 	Za usmereni graf \textit{$G=(V,E)$}, neka je \textit{$G^{'}=(V,E^{'})$}, gde je 
 	\textit{$E^{'} = E - \{ (v,v) | v \in V \}$}, tj. \textit{$G^{'}$} nastaje od grafa \textit{G}
 	uklanjanjem petlji. Neka je \textit{$E^{n} = \{\{a,b\} | (a,b) \in E^{'} \lor (b,a) \in E^{'} \}$}.
 	Tada neusmereni graf \textit{$G^{n}=(V,E^{n})$} nazivamo noseći graf grafa G.
\end{definition}

Neformalno, skup grana \textit{$E^{n}$} je definisan kao \textit{$\{a,b\} \in E^{n}$}, za različite čvorove \textit{a} i \textit{b}, 
ako i samo ako \textit{$(a,b) \in G$} ili \textit{$(b,a) \in G$}.

 \begin{definition}
	Usmereni graf \textit{$G=(V,E)$} je povezan ako je njegov noseći graf povezan. Usmereni graf je \textit{jako povezan} ako za svaki par čvorova \textit{$a,b \in V$}
	postoji usmereni put od čvora \textit{a} do čvora \textit{b}.
\end{definition}

\section{Izomorfizam grafova}
\hspace{\parindent} Jedan od izazova koji se pojavljuje prilikom generisanja misija pomoću grafovskih gramatika 
jeste pronalaženje izomorfnih podgrafa na osnovu datog pravila gramatike. Razlog za to u našem slučaju je
primena istog pravila prezapisivanja na pronađeni podgraf. O pravilima i gramatikama će biti više reči u daljem tekstu, dok ćemo se sada baviti definisanjem
grafovskog izomorfizma i opisivanjem algoritma za njegovo pronalaženje.

\subsection{Definicije i problem izomorfizma podgrafa}

\begin{definition}
	Za date grafove \textit{$G=(V,E)$} i \textit{$G^{'}=(V^{'},E^{'})$}, \textit{morfizam} u oznaci \textit{$f \colon G \rightarrow G^{'} $}, \textit{$f= (f_V,f_E)$} se sastoji
	od dve funckije \textit{$f_V \colon V \rightarrow V^{'} $} i \textit{$f_E \colon E \rightarrow E^{'} $}.
	Morfizam \textit{f} je injektivan (ili surjektivan) ako su obe funkcije \textit{$f_V$} i \textit{$f_E$} injektivne (ili surjektivne). 
\end{definition}

\begin{definition}
	Funkcija \textit{f} koja graf \textit{$G=(V,E)$} preslikava na graf \textit{$G^{'}=(V^{'},E^{'})$} naziva se \textit{homomorfizmom} iz \textit{G} u \textit{$G^{'}$},
	u oznaci \textit{$f \colon G \rightarrow G^{'}$}, ako važe sledeća svojstva:
	\begin{enumerate}
		\item Ako je \textit{$v \in V$}, tada \textit{$f(v) \in V^{'}$}, odnosno \textit{$f(V) \subseteq f(V^{'})$}.
		\item Ako je \textit{$e \in E$}, tada \textit{$f(e) \in E^{'}$}, odnosno \textit{$f(E) \subseteq f(E^{'})$}.
		\item Ako su čvorovi \textit{a} i \textit{b} incidentni grani \textit{e} u grafu \textit{G}, tada su \textit{f(a)} i \textit{f(b)} incidentni
		grani \textit{f(e)} u grafu \textit{$G^{'}$}.
	\end{enumerate}	
\end{definition}

 \begin{definition}
 	Homomorfizam \textit{$f \colon G \rightarrow G^{'}$} je \textit{izomorfizam} ako su \textit{$f \colon V \rightarrow V^{'}$}
 	i \textit{$f \colon E \rightarrow E^{'}$} bijektivna preslikavanja. Ako je \textit{$f \colon G \rightarrow G^{'}$} \textit{izomorfizam},
 	tada se za grafove \textit{$G$} i \textit{$G^{'}$} kaže da su \textit{izomorfni}.
\end{definition}

Intuitivno, izomorfizam se može razmatrati kao preimenovanje čvorova i grana grafa \textit{G}. Pošto ćemo koristiti grafove sa labelama,
u našoj implemetaciji će od interesa biti samo grafovi čiji čvorovi imaju labele dok grane neće nositi oznake.
Stoga nas interesuje preimenovanje čvorova, a za grane će dovoljan uslov biti da postoje između odgovarajućih čvorova.

\begin{figure}[H]
	\centering
	\includegraphics[width=150mm]{images/Isomorphism}
	\caption{Primer dva izomorfna grafa.}
\end{figure}

Problem pronalaženja izomorfnog podgrafa definišemo na sledeći način: ako su dati grafovi \textit{G}
i \textit{H}, potrebno je pronaći podgraf grafa \textit{G} (ili sve takve podgrafove) koji je izomorfan grafu H.
Za neke izbore grafova \textit{G} i \textit{H} moguće je da postoji eksponencijalni broj pojavljivanja \textit{H} u \textit{G}. 
U opštem slučaju ovo je NP-kompletan problem \cite{complexity}. 
Zanimljivo je spomenuti i problem provere da li su dva fiksirana grafa \textit{G} i \textit{H} izomorfna, 
koji predstavlja specijalan slučaj problema pronalaženja izomorfnog podgrafa. Za ovaj problem nije poznato da li se može rešiti u polinomskom vremenu, ali takođe nije dokazano ni da je NP kompletan.
Ipak, za mnoge specijalne vrste grafova, kao što su stabla i planarni grafovi, ovaj problem se može rešiti u polinomskom vremenu. 

Implementacija algoritma \textit{VF2} \cite{subgraphisomorphism} za pronalaženje izomorfizma u 
podgrafu je korišćena tokom razvoja i u daljem tekstu sledi njen kratak opis i pseudokod.

\subsection{Algoritam VF2}

Za date grafove \textit{$G_1=(N_1,B_1)$} i \textit{$G_2=(N_2,B_2)$}, cilj izvršavanja algoritma \textit{VF2} je da pronađe pojavljivanje grafa 
\textit{$G_2$} unutar grafa \textit{$G_1$}.
Proces pronalaženja se odvija kroz postepenu izgradnju preslikavanja \textit{M} koje uparuje čvorove iz \textit{$G_1$} sa čvorovima iz \textit{$G_2$}
pod određenim predefinisanim uslovima. Preslikavanje \textit{M} je izraženo skupom uređenih parova
\textit{$(v_1,v_2)$}, tako da \textit{$v_1 \in G_1$} i \textit{$v_2 \in G_2$}. Preslikavanje \textit{$M \subset N_1 \times N_2$} je izomorfizam 
ako i samo ako je \textit{M} bijektivna funkcija koja čuva strukturu grafa.
Preslikavanje \textit{$M \subset N_1 \times N_2$} je graf-podgraf izomorfizam ako i samo ako je \textit{M}
izomorfizam između grafa \textit{$G_2$} i podgrafa od \textit{$G_1$}.

Algoritam \textit{VF2} postepeno gradi konačno rešenje tako što u svakoj iteraciji dodaje odgovarajuće čvorove i grane
parcijalnom rešenju preslikavanja. Koristićemo stanja, gde svako stanje \textit{s} može biti pridruženo parcijalnom rešenju preslikavanja, u oznaci  \textit{M(s)}. Stanje predstavlja skup uređenih parova \textit{$(v_1,v_2)$} takvih da 
\textit{$v_1$} pripada \textit{$G_1$} i \textit{$v_2$} pripada \textit{$G_2$}, gde uredjeni par predstavlja preslikavanje
čvora iz grafa \textit{$G_1$} u čvor grafa \textit{$G_2$}.
\textit{M(s)} je podskup kompletnog rešenja preslikavanja \textit{M}.
Sa \textit{M(s)} identifikuju se dva podgrafa od \textit{$G_1$} i \textit{$G_2$}.
Podgrafovi \textit{$G_1(s)$} i \textit{$G_2(s)$} se dobijaju odabirom čvorova iz \textit{$G_1$} i \textit{$G_2$}
koji se nalaze u \textit{M(s)} i grana koje ih povezuju.
Sa \textit{$M_1(s)$}, \textit{$M_2(s)$}, \textit{$B_1(s)$} i \textit{$B_2(s)$} označavamo skupove
čvorova grafova \textit{$G_1(s)$} i \textit{$G_2(s)$} i njihovih odgovarajućih grana.
Posmatrano na ovaj način, prelaz iz stanja \textit{s} u stanje \textit{$s^{'}$} predstavlja dodavanje uparenih čvorova
\textit{$(v_1,v_2)$} parcijalnim grafovima definisanim stanjem \textit{s}.

\lstset{language=Pascal,mathescape,         
	literate={->}{$\rightarrow$}{2}
	{ε}{$\varepsilon$}{1},
	inputencoding=utf8,
}    

\begin{lstlisting}[caption=Pseudokod algoritama VF2]
PROCEDURE Match(s)
  INPUT: trenutno stanje s; M(s) za inicijalno stanje s je prazan
  OUTPUT: preslikavanje izme(*@đ@*)u dva grafa
	
  IF M(s) pokriva sve (*@č@*)vorove u G$_2$ THEN
    OUTPUT M(s)
  ELSE
    Na(*@đ@*)i skup parova P(s) takav da su kandidati za dodavanje u M(s)
    FOREACH p in P(s)
      IF pravila izvodljivosti su ispunjena za dodavanje p u M(s) 
      THEN
        Napravi stanje $s^{'}$ dobijeno dodavanjem p u M(s)
        CALL Match($s^{'}$)
      END IF
    END FOREACH
    Povrati strukture podataka
  END IF
END PROCEDURE Match
	
\end{lstlisting}

Samo mali skup stanja stanja \textit{s} je \textit{konzistentan} sa traženim morfizmom.
Odnosno, postoje stanja \textit{s} koja u daljem izvođenju ne dovode do traženog rešenja.
Primenom određenih pravila možemo suziti prostor pretrage.
Algoritam uvodi skup pravila koja održavaju konzistentnost prilikom prelaska iz stanja \textit{s} u stanje 
\textit{$s^{'}$}. 
Ova pravila se nazivaju \textit{pravila izvodljivosti}. Koristićemo \textit{funkciju izvodljivosti} 
\textit{$F(s,v_1,v_2)$} koja ima logičku vrednost \textit{tačno} ako par \textit{$(v_1,v_2)$} zadovoljava sve
uslove izvodljivosti. Pošto nam je potrebno da posmatramo i atribute grafova, a ne samo njihovu strukturu, 
tada funkcija izvodljivosti dobija oblik: \textit{$F(s,v_1,v_2) = F_{syn}(s,v_1,v_2) \land F_{sem}(s,v_1,v_2)$}, 
gde \textit{$F_{syn}$} zavisi od strukture, a \textit{$F_{sem}$} zavisi od atributa. 
\textit{$F_{syn}$} se naziva \textit{sintaksička}, a \textit{$F_{sem}$} \textit{semantička} izvodljivost. 

U inicijalnom stanju \textit{$s_0$}, funkcija preslikavanja ne sadrži nijednu komponentu, 
\textit{$M(s_0)=\emptyset$}. Za svako međustanje \textit{s}, algoritam pronalazi skup 
uređenih parova čvorova \textit{P(s)} koji su kandidati za dodavanje u trenutno stanje
\textit{s}. Skup kandidata se može suziti primenom funkcije izvodljivosti za svaki par \textit{p} koji pripada \textit{P(s)}.
Ako funkcija daje logičku vrednost \textit{tačno}, onda je naredno stanje
\textit{$s^{'}= s \cup p $}, gde je \textit{$p=(v_1,v_2)$}. Ceo proces se rekurzivno ponavlja za
\textit{$s^{'}$}. Graf se na ovaj način obilazi kao po algoritmu \textit{DFS}.

Skup parova kandidata \textit{$P(s)$} se dobija unijom dva skupa čvorova.
Prvi skup sadrži čvorove koji su povezani sa \textit{$G_1(s)$} i 
\textit{$G_2(s)$}. Označimo sa \textit{$T^{out}_{1}(s)$} i \textit{$T^{out}_{2}(s)$} 
skupove čvorova koji nisu u parcijalnom preslikavanju, ali su krajevi grana koje polaze iz  
\textit{$G_1(s)$} i \textit{$G_2(s)$}. 
Slično, sa \textit{$T^{in}_{1}(s)$} i \textit{$T^{in}_{2}(s)$}
označavamo skupove čvorova, koje nisu u parcijalnom preslikavanju, ali čiji su krajevi 
grana u \textit{$G_1(s)$} i \textit{$G_2(s)$}.
\textit{P(s)} se sastoji od uređenih parova \textit{$(v_1,v_2)$}, gde \textit{$v_1$} pripada skupu 
\textit{$T^{out}_{1}(s)$} ili skupu \textit{$T^{in}_{1}(s)$}, dok \textit{$v_2$} pripada skupu
\textit{$T^{out}_{2}(s)$} ili skupu \textit{$T^{in}_{2}(s)$}.

Pravila izvodljivosti proveravaju validnost kandidata koji potencijalno ulazi u rešenje.
Proveravamo da li su čvorovi koji se dodaju rešenju na sličan način povezani u odgovarajućim grafovima \textit{$G_1$} i \textit{$G_2$} sa ostalim čvorovima u ta dva grafa.
Potrebno je uporediti ulazne i izlazne stepene čvorova oba grafa.
Ako za datog kandidata \textit{$(v_1,v_2) \in P(s)$} važi da je izlazni stepen čvora \textit{$v_2 \in G_2$} veći od izlaznog stepena čvora \textit{$v_1 \in G_1$} tada je kandidat \textit{$(v_1,v_2)$} nevalidan.
Slično, kandidat se validira i prema ulaznim stepenima čvorova.
Kada je u pitanju semantička provera možemo uzimati u obzir labele grana i labele čvorova.
Labele mogu biti numeričke ili simboličke vrednosti. U našem slučaju prilikom provere
semantičke izvodljivosti mi ćemo obraćati pažnju samo na labele čvorova, dok će za grane biti dovoljno da povezuju odgovarajuće čvorove, a njihove labele nećemo uzimati u obzir.

\subsubsection{Primer izvršavanja algoritma VF2}
Ilustrujemo jednu iteraciju izvršavanja algoritma VF2 nad grafovima \textit{G} i \textit{H}
datim na slici \ref{vf2example}, tako da tražimo graf \textit{H} kao podgraf grafa \textit{G}.

Plavom, žutom i zelenom bojom označena su preslikavanja čvorova. 
U primeru polazimo od toga da je preslikavanje čvora \textit{$g_1$} u čvor \textit{$h_1$} već dodato u skup
parova preslikavanja.
Tokom jedne iteracije izvršavaju se naredni koraci:
\begin{enumerate}
	\item Skup uređenih parova preslikavanja \textit{M(s)} trenutno sadrži samo uređeni par \textit{$(g_1, h_1)$}. Algoritam nije još pronašao graf \textit{H} u grafu \textit{G}, nastavlja se sa pretragom.
	
	\item Pronalazimo skup uređenih parova \textit{P(s)} koji predstavljaju skup kandidata za dodavanje u preslikavanje \textit{M}.
	Tražimo skupove čvorova koji su povezani sa čvorovima iz \textit{G(s)} i \textit{H(s)}.
	\begin{itemize}
	\item U skup \textit{$T_G^{out}(s)$} stavljamo čvorove koji nisu u parcijalnom rešenju, ali su krajevi grana koji polaze iz \textit{G(s)}.
		  Dodajemo čvorove \textit{$g_2$}, \textit{$g_3$} i \textit{$g_4$}.
	\item U skup \textit{$T_H^{out}(s)$} stavljamo čvorove koji nisu u parcijalnom rešenju, ali su krajevi grana koji polaze iz \textit{H(s)}.
		  Dodajemo čvorove \textit{$h_2$} i \textit{$h_3$}.
	\item U skup \textit{$T_G^{in}(s)$} stavljamo čvorove koji nisu u parcijalnom rešenju, ali čiji krajevi grana završavaju u \textit{G(s)}.
		  Primer nema ovakvih čvorova, skup ostaje prazan.
	\item U skup \textit{$T_H^{in}(s)$} stavljamo čvorove koji nisu u parcijalnom rešenju, ali čiji krajevi grana završavaju u \textit{H(s)}.
		  Primer nema ovakvih čvorova, skup ostaje prazan.
	\end{itemize}
	Konstruišemo \textit{P(s)} dodavanjem uređenih parova \textit{$(v_1,v_2)$}, gde \textit{$v_1$} pripada skupu \textit{$T_G^{out}(s)$} ili \textit{$T_G^{in}(s)$}, dok \textit{$v_2$} 
	pripada skupu \textit{$T_H^{out}(s)$} ili \textit{$T_H^{in}(s)$}. Naš skup sadrži naredne elemente: 
	\textit{$(g_2,h_2)$}, \textit{$(g_2,h_3)$}, \textit{$(g_3,h_2)$}, \textit{$(g_3,h_3)$}, \textit{$(g_4,h_2)$} i \textit{$(g_4,h_3)$}. 
	
	\item U ovako dobijenom skupu \textit{P(s)} tražimo kandidata za dodavanje u preslikavanje \textit{M} i na taj način prelazimo iz stanja \textit{s} u stanje \textit{$s^{'}$}.
	Nad svakim kandidatom vršimo proveru pozivanjem funkcije izvodljivosti koja daje vrednost \textit{tačno} za par koji se može dodati preslikavanju.
	Osnovni kriterijumi funkcije su broj ulaznih i izlaznih grana, kao i pravilan smer grana za čvorove u paru.
	
	\item Prvi validan par je \textit{$(g_2,h_2)$} koji se dodaje stanju \textit{s} i time dobijamo stanje \textit{$s^{'}$}. Naredna iteracija se izvršava za parcijalno preslikavanje \textit{$M(s^{'})$}. 
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[width=150mm]{images/VF2}
	\caption{Grafovi \textit{G} i \textit{H}.}
	\label{vf2example}
\end{figure}



